export default class Earthdawn4eActorSheet extends ActorSheet {
  takeDamageDialog = `
          <div style="float: left">
              <label>${game.i18n.localize('earthdawn.d.damage')}: </label>
              <input id="damage_box" value=0 autofocus/>
          </div>
          <div>
              <label>${game.i18n.localize('earthdawn.t.type')}: </label>
              <select id="type_box">
                <option value="physical">Physical</option>
                <option value="mystic">Mystic</option>
              </select>
          </div>
          <div>
            <label>${game.i18n.localize('earthdawn.i.ignoreArmor')}?</label>
            <input type="checkbox" id="ignore_box"/>
          </div>`;

  baseListeners(html) {
    super.activateListeners(html);

    $(document).on('keydown', 'form', function (ev) {
      return ev.key !== 'Enter';
    });

    html.find('.item-delete').click(async (ev) => {
      let li = $(ev.currentTarget).parents('.item-name'),
          itemId = li.attr('data-item-id');
      let confirmationResult = await this.confirmationBox();
      if (confirmationResult.result === false){
        return false
      }
      else{
        this.actor.deleteEmbeddedDocuments('Item', [itemId]);
      }
    });

    html.find('.item-edit').click((ev) => {
      const li = $(ev.currentTarget).parents('.item-name');
      const item = this.actor.items.get(li.data('itemId'));
      item.sheet.render(true);
    });

    html.find('.link-checkbox-effect').click(async (ev) => {
      event.preventDefault();

      const li = $(ev.currentTarget).parents('.item-name');
      const item = this.actor.effects.get(li.data('itemId'));
      let visibleState = event.target.checked;
      let disabledState = !visibleState;

      await item.update({ 'disabled': disabledState });
     
    });

    
    html.find('.effect-delete').click(async (ev) => {
      let li = $(ev.currentTarget).parents('.item-name'),
          itemId = li.attr('data-item-id');
      let confirmationResult = await this.confirmationBox();
      if (confirmationResult.result === false){
        return false
      }
      else{
        this.actor.deleteEmbeddedDocuments('ActiveEffect', [itemId]);
      }
    });

    html.find('.effect-add').click(() => {
    
      let itemNumber = this.actor.data.effects.size;
      let itemData = {label: `New Effect ` + itemNumber,
                      icon: "systems/earthdawn4e/assets/effect.png",
                      duration: {rounds: 1},
                      origin: this.actor.id
                    }

      this.actor.createEmbeddedDocuments("ActiveEffect", [itemData])
    });
	
    html.find('.effect-edit').click((ev) => {
      const li = $(ev.currentTarget).parents('.item-name');
      const item = this.actor.effects.get(li.data('itemId'));
      item.sheet.render(true);
    });
    
    html.find('.NPC-attack').click((ev) => {
      const li = $(ev.currentTarget).parents('.item-name');
      const item = this.actor.items.get(li.data('itemId'));
      let itemID = li.data('itemId');
      const modifier = 0;
      const strain = item.data.data.strain ? item.data.data.strain : 0;
      const karma = 0;
      
      let type = (item.data.data.powerType === "Attack") ? "attack" : (item.data.data.attackstep > 0) ? "test" : "";
      const parameters = {
        itemID: itemID,
        steps: item.data.data.attackstep,
        talent: item.name,
        strain: strain,
        type: type,
        karma: karma,
        modifier: modifier,
      };
      this.actor.NPCtest(parameters);
    });

    html.find('.knockdown-test').click(() => {
      let inputs = {};
      this.actor.knockdownTest(inputs);
    });

    html.find('.jump-up').click(() => {
      this.actor.jumpUpTest();
    });

    html.find('.take-damage').click(async () => {
      let inputs = await new Promise((resolve) => {
        new Dialog({
          title: game.i18n.localize('earthdawn.t.takeDamage'),
          content: this.takeDamageDialog,
          buttons: {
            ok: {
              label: game.i18n.localize('earthdawn.o.ok'),
              callback: (html) => {
                resolve({
                  damage: html.find('#damage_box').val(),
                  type: html.find('#type_box').val(),
                  ignore: html.find('#ignore_box:checked'),
                });
              },
            },
          },
          default: 'ok',
        }).render(true);
      });

      inputs.ignorearmor = inputs.ignore.length > 0;
      this.actor.takeDamage(inputs);
    });

    html.find('.item-display').click((ev) => {
      const li = $(ev.currentTarget).parents('.item-name');
      const item = this.actor.items.get(li.data('itemId'));

      let html = `
                <img src="${item.img}" title="${item.name}" height="64" width="64"/>
                <div>Name: ${item.name}</div>`;

      if (item.data.data.damagestep) {
        html += `<div>Damage Step: ${item.data.data.damagestep}</div>`;
      }

      if (item.data.data.damagestep) {
        html += `<div>Damage Step: ${item.data.data.damagestep}</div>`;
      }

      html += `<div>${item.data.data.description}</div>`;

      const chatData = {};
      let rollMode = game.settings.get('core', 'rollMode');

      if (['gmroll', 'blindroll'].includes(rollMode)) chatData['whisper'] = ChatMessage.getWhisperRecipients('GM');
      if (rollMode === 'blindroll') chatData['blind'] = true;

      ChatMessage.create({
        content: html,
        whisper: chatData.whisper,
        blind: chatData.blind,
        speaker: ChatMessage.getSpeaker({ alias: this.actor.name }),
      });
    });

    html.find('.item-upgrade').click((ev) => {
      const li = $(ev.currentTarget).parents('.item-name');
      const item = this.actor.items.get(li.data('itemId'));
      if(item.data.data.ranks) {
        const rank = item.data.data.ranks + 1;
        item.update({
          'data.ranks': rank
        });
      } else if (item.data.data.circle) {
        const circle = item.data.data.circle + 1;
        item.update({
          'data.circle': circle
        });
      }
    });

    
    html.find('.item-downgrade').click((ev) => {
      const li = $(ev.currentTarget).parents('.item-name');
      const item = this.actor.items.get(li.data('itemId'));
      if(item.data.data.ranks) {
        const rank = item.data.data.ranks - 1;
        item.update({
          'data.ranks': rank
        });
      } else if (item.data.data.circle !== undefined) {
        const circle = item.data.data.circle - 1;
        item.update({
          'data.circle': circle
        });
      }
    });

    html.find('.show-hidden').click((event) => this._showItemDescription(event));
  }

  activateListeners(html) {
    super.activateListeners(html);

    html.find('.spell-roll').click((ev) => {
      const li = $(ev.currentTarget).parents('.item-name');
      let itemID = li.data('itemId');
      const spell = this.actor.items.get(itemID);
      this.actor.castSpell(spell);
    });
  }
  async confirmationBox(){
    return await new Promise((resolve) => {
      new Dialog({
        title: `Confirm Delete`,
        content: `Are You Sure?
          
              `,
        buttons: {
          ok: {
            label: game.i18n.localize('earthdawn.o.ok'),
            callback: (html) => {
              resolve({
                result: true,
              });
            },
          },
          cancel: {
            label: game.i18n.localize('earthdawn.c.cancel'),
            callback: (html) =>{
              resolve({
                result: false,
              });
            }
          }
        },
        default: 'ok',
      }).render(true);
    });


  }


  /** @override */
  async _onDropItemCreate(itemData) {

    console.debug("ED4E:\tIn _onDropItemCreate");

    let itemType = itemData.type;

    console.debug(`ED4E:\tType of dropped item: ${itemType}`);

    // Increment the number of a discipline circle  or talent/skill rank of a character instead of creating a new item
    if ( itemType === "discipline" || itemType === "talent" || itemType === "skill") {

      console.debug(`ED4E:\tDropped ${itemType} item`);

      const levelAttributeName = itemType === "discipline" ? "circle" : "ranks";
      const droppedItem = this.actor.itemTypes[itemType].find(c => c.name === itemData.name);
      let priorLevel = Number.parseInt(droppedItem?.data.data[levelAttributeName]) || 0;
      if ( !!droppedItem ) {

        console.debug(`ED4E:\tFound corresponding ${itemType} already existing on actor`);
        console.debug(`ED4E:\tCurrent ${itemType} level: ${priorLevel}`);

        const next = priorLevel + 1;
        if ( next > priorLevel ) {
          return droppedItem.update({[`data.${levelAttributeName}`]: String(next)});
        }
      }
    }

    console.debug("ED4E:\tNo corresponding discipline found. Add new item.");
    // Default drop handling if levels were not added
    return super._onDropItemCreate(itemData);
  }

  _showItemDescription(event) {
    event.preventDefault();
    const toggler = $(event.currentTarget);
    const item = toggler.parents('.showHidden');
    const description = item.find('.hidden-tab');

    $(description).slideToggle(function () {
      $(this).toggleClass('open');
    });
  }

  
}
