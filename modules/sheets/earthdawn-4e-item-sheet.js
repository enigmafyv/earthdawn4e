export default class earthdawn4eItemSheet extends ItemSheet {
  // talent sheet
  static get defaultOptions() {
    return mergeObject(super.defaultOptions, {
      width: 500,
      height: 500,
      //the "earthdawn", "sheet", "talent" is something to point the stylesheet to for identification
      classes: [
        'earthdawn',
        'sheet',
        'discipline',
        'talent',
        'weapon',
        'skill',
        'spell',
        'attacks',
        'armor',
        'threadItem',
        'equipment',
        'namegiver',
        'shield',
        'thread',
        'knack',
        'mask',
        'devotion',
      ],
      tabs: [
        {
          navSelector: '.tabs',
          contentSelector: '.sheet-body',
          initial: 'description',
        },
      ],
    });
  }

  get template() {
    return `systems/earthdawn4e/templates/items/sheets/${this.item.data.type}-sheet.hbs`;
  }

  getData() {
    const data = super.getData();
    const itemData = data.data;
    data.item = itemData;
    data.data = itemData.data;
    data.parentTalents = this.item.isOwned ? this.item.parent.items.filter((item) => item.type === 'talent') : [];
    data.isGM = game.users.current.isGM;
    console.log('[EARTHDAWN] Item Sheet', data);
    return data;
  }

  activateListeners(html) {
    super.activateListeners(html);
    html.find('.immediate-update-threads').on('input', () => {
      if (this.item.parent instanceof Actor){
        ui.notifications.error("Do Not Adjust Threads on an Owned Item")
        return
      }
      let newthreads = document.getElementsByName('data.numberthreads')[0].value;
      this.item.update({ 'data.numberthreads': newthreads }, {render: false});
    });

    html.find('.change-thread-number').click(async () => {
      if (this.item.parent instanceof Actor){
        ui.notifications.error("Do Not Adjust Threads on an Owned Item")
        return
      }
      let threadnumber = this.item.data.data.numberthreads;
      const newThreads = {}
      for (let i = 1; i <= threadnumber; i ++) {
        newThreads[`rank${i}`] = { 
        keyknowledge: '',
        testknowledge: '',
        deed: '',
        effect: '',
        characteristic: '',
        value: 0,
        threadactive: false,
        hide: false, 
      };
    }

      await this.item.update({'data.threads': null})
      await this.item.update({ 'data.threads': newThreads })
      });


      }
  }

