import earthdawn4eNPCSheet from '../earthdawn-4e-NPC-sheet.js';

export class TakaCreatureSheet extends earthdawn4eNPCSheet {
  /** @override */
  static get defaultOptions() {
    return mergeObject(super.defaultOptions, {
      classes: ['taka-theme'],
      width: 790,
      height: 750,
      tabs: [
        {
          navSelector: '.tabs',
          contentSelector: '.sheet-body',
          initial: 'description',
        },
      ],
    });
  }

  /** @override */
  get template() {
    return `systems/earthdawn4e/templates/taka-theme/actors/creature-sheet.hbs`;
  }

  /* -------------------------------------------- */
}
