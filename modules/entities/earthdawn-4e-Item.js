export default class earthdawn4eItem extends Item {
  prepareData() {
    super.prepareData();
    const itemData = this.data;
    if (this.type === 'spellmatrix') {
      this._prepareItemData(itemData);
    } else if (this.type === 'thread' && this.isEmbedded === true) {
      this._prepareThreadData(itemData);
    } 
    if (this.type === 'weapon' || this.type === 'armor'){
      this._prepareForgedItems(itemData)
    }
  }

  _prepareItemData(itemData) {
    const data = itemData.data;
    data.totalthreads = this.threadCount();
    data.totalthreadsneeded = Number(data.threadsrequired) + Number(data.targetadditionalthreads);
  }
  _prepareThreadData(itemData) {}
  threadCount() {
    let totalThreads;
    totalThreads = Number(this.data.data.constantthreads) + Number(this.data.data.activethreads);
    return totalThreads;
  }

  _prepareForgedItems(itemData) {
    const data = itemData.data
    if (itemData.type === "weapon"){
    data.damageFinal = Number(data.damagestep) + Number(data.timesForged)
    }
    else {
      data.physicalArmorFinal = Number(data.Aphysicalarmor) + Number(data.timesForgedPhysical)
      data.mysticArmorFinal = Number(data.Amysticarmor) + Number(data.timesForgedMystic)

    }
  }
}
