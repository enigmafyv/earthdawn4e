export default class earthdawn4eCombat extends Combat {
  async startCombat() {
    await this.setupTurns();
    console.log('Custom Combat has started');
    return super.startCombat();
  }

  async nextRound() {
    await game.combat.resetAll();

    await new Promise((resolve) => {
      new Dialog({
        title: game.i18n.localize('earthdawn.n.newTurn'),
        content: game.i18n.localize('earthdawn.i.initiativeReminder'),
        buttons: {
          ok: {
            label: game.i18n.localize('earthdawn.o.ok'),
            callback: () => {
              resolve({});
            },
          },
        },
        default: game.i18n.localize('earthdawn.o.ok'),
      }).render(true);
    });

    const combatants = this.data.combatants;
    for (const element of combatants) {
      await game.combat.rollAll();
      await game.combat.setupTurns();
      console.log('New Round Has Started');
      return super.nextRound();
    }
  }
}
