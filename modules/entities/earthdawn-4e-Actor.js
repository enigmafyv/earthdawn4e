import { chatOutput } from '../helpers/chat-message.js';
import { optionBox } from '../helpers/option-box.js';
import { karmaAllowed } from '../helpers/karma-allowed.js';
import { devotionAllowed } from '../helpers/devotion-allowed.js';
import { get1eDice } from '../helpers/get-1e-dice.js';
import { get3eDice } from '../helpers/get-3e-Dice.js';
import { getCeDice } from '../helpers/get-ce-dice.js';

export default class earthdawn4eActor extends Actor {
  prepareData() {
    this.data.reset();
    this.prepareBaseData();
    
    const actorData = this.data;
    if (actorData.type === 'pc') this._prepareCharacterData(actorData);
    if (actorData.type === 'npc') this._prepareNPCData(actorData);
    if (actorData.type === 'creature') this._preparecreatureData(actorData);
    
    this.prepareEmbeddedDocuments();
    this._prepareDerivedData(actorData);

  }
  _prepareDerivedData(actorData){
    if (actorData.type === 'pc'){
    const data = actorData.data;
    this.threadBonusCount();
    data.physicalarmor = this.armorCount('Aphysicalarmor') + +this.permanentMod('physicalarmor') + data.overrides.physicalarmor;
    data.mysticarmor =
      this.armorCount('Amysticarmor') +
      this.permanentMod('mysticarmor') +
      Math.floor(data.attributes.willpowervalue / 5) +
      data.overrides.mysticarmor;
    data.initiativeDice = this.getDice(data.initiativeStep);
    data.initiativeRoll = this.explodify(data.initiativeDice);
    data.unconsciousleft.max = data.unconscious.max;
    data.unconsciousleft.value = data.unconscious.max - data.damage.value;
    }
  }

  _prepareCharacterData(actorData) {
    let totaldurability = this.getDurability();
    const data = actorData.data;
    data.spells = this.getSpells();
    data.dexterityStep = this.getStep(data.attributes.dexterityvalue);
    data.dexterityDice = this.getDice(data.dexterityStep);
    data.strengthStep = this.getStep(data.attributes.strengthvalue);
    data.strengthDice = this.getDice(data.strengthStep);
    data.toughnessStep = this.getStep(data.attributes.toughnessvalue);
    data.toughnessDice = this.getDice(data.toughnessStep);
    data.perceptionStep = this.getStep(data.attributes.perceptionvalue);
    data.perceptionDice = this.getDice(data.perceptionStep);
    data.willpowerStep = this.getStep(data.attributes.willpowervalue);
    data.willpowerDice = this.getDice(data.willpowerStep);
    data.charismaStep = this.getStep(data.attributes.charismavalue);
    data.charismaDice = this.getDice(data.charismaStep);
    data.bloodMagicDamage = this.bloodMagicCount() + data.overrides.bloodMagicDamage;
    data.bloodMagicWounds = 0 + data.overrides.bloodMagicWounds;
    data.unconsciousthreshold = data.attributes.toughnessvalue * 2;
    data.deathThreshold = data.unconsciousthreshold + data.toughnessStep;
    data.woundsCurrent = data.wounds + data.bloodMagicWounds;
    data.physicaldefense =
      Math.floor((data.attributes.dexterityvalue + 3) / 2) +
      data.overrides.physicaldefense +
      this.permanentMod('physicaldefense') +
      this.defenseCount('physicaldefense') +
      this.physicalDefensemod();
    data.mysticdefense =
      Math.floor((data.attributes.perceptionvalue + 3) / 2) +
      data.overrides.mysticdefense +
      this.permanentMod('mysticdefense') +
      this.defenseCount('mysticdefense') +
      this.mysticDefensemod();
    data.socialdefense =
      Math.floor((data.attributes.charismavalue + 3) / 2) + this.permanentMod('socialdefense') + data.overrides.socialdefense;
    data.initiativeStep = data.dexterityStep - this.armorInitiativePenalty() + this.permanentMod('initiative') + this.initiativeMod() - data.wounds;
    data.dailytests = Math.ceil(data.attributes.toughnessvalue / 6);
    data.karma.max = this.karmaCalc() + data.unspentattributepoints;
    data.devotion.max = this.devotionCalc();
    data.woundThreshold = Math.ceil((data.attributes.toughnessvalue + 4) / 2) + this.permanentMod('woundthreshold');
    data.woundThreshold += data.overrides.woundthreshold ? data.overrides.woundthreshold : 0;
    data.durability = totaldurability.runningtotal;
    data.durability2 = data.durability;
    data.recoverytestsrefresh = Math.ceil(data.attributes.toughnessvalue / 6);
    data.unconscious.max = data.unconsciousthreshold + data.durability + data.overrides.unconsciousrating + this.permanentMod('durability')+ totaldurability.threadDurability - data.bloodMagicDamage;
    data.damage.max =
      data.deathThreshold + data.durability + totaldurability.totalcircles + totaldurability.threadDurability + this.permanentMod('durability') + data.overrides.deathrating - data.bloodMagicDamage;
    data.unconscious.value = data.damage.value;
    data.encumbrance = this.carryingCount();
    data.carryingCapacity = this.carryingCapacity() + this.permanentMod('carryingcapacity');
    data.movementFinal = Number(data.movement) + Number(data.overrides.movement);
    data.recoverytestsrefreshFinal = data.recoverytestsrefresh + data.overrides.recoverytestsrefresh + this.permanentMod('recovery');
  }

  _prepareNPCData(actorData) {
    const data = actorData.data;
    data.modifiedInitiative = Number(data.initiativeStep) - Number(data.wounds);
    data.initiativeDice = this.getDice(data.modifiedInitiative);
    data.initiativeRoll = this.explodify(data.initiativeDice);
    data.damage.max = data.deathThreshold;
    data.unconscious.max = data.unconsciousThreshold;
    data.dexterityDice = this.getDice(data.dexterityStep);
    data.strengthDice = this.getDice(data.strengthStep);
    data.toughnessDice = this.getDice(data.toughnessStep);
    data.perceptionDice = this.getDice(data.perceptionStep);
    data.willpowerDice = this.getDice(data.willpowerStep);
    data.charismaDice = this.getDice(data.charismaStep);
    data.unconsciousleft.max = data.unconsciousThreshold;
    data.unconsciousleft.value = data.unconsciousThreshold - data.damage.value;

  }

  _preparecreatureData(actorData) {
    const data = actorData.data;
    data.modifiedInitiative = Number(data.initiativeStep) - Number(data.wounds);
    data.initiativeDice = this.getDice(data.modifiedInitiative);
    data.initiativeRoll = this.explodify(data.initiativeDice);
    data.damage.max = data.deathThreshold;
    data.unconscious.max = data.unconsciousThreshold;
    data.unconsciousleft.max = data.unconsciousThreshold;
    data.unconsciousleft.value = data.unconsciousThreshold - data.damage.value;
  }

  threadBonusCount() {
    const data = this.data;
    const talents = data.items.filter(function (item) {
      return item.type === 'talent';
    });
    const threads = data.items.filter(function (item) {
      return item.type === 'thread' && item.data.data.active === true;
    });
    for (const element of talents) {
      let matchingThread = threads.filter(function (thread) {
        return thread.data.data.characteristic === element.id;
      });
      let talentBonus = 0;
      if (matchingThread.length > 0) {
        talentBonus = matchingThread[0].data.data.rank;
        console.log("Thread Matches")
      }
      element.data.data.finalranks = Number(element.data.data.ranks) + Number(talentBonus);
    }
  }

  armorCount(type) {
    const data = this.data;
    const armor = data.items.filter(function (item) {
      return item.type === 'armor' && item.data.data.worn === true;
    });

    let runningtotal = 0;
  
    let namegiver = this.items.filter(function (item) {
      return item.type === 'namegiver';
    });
    if (namegiver.length > 0 && game.i18n.localize(namegiver[0].data.name) === 'Obsidiman' && type === 'Aphysicalarmor') {
      runningtotal += 3;
    }
    for (const element of armor) {
      if (type === 'Aphysicalarmor'){
        
        runningtotal += element.data.data.physicalArmorFinal
      }
      else if (type === 'Amysticarmor'){
        runningtotal += element.data.data.mysticArmorFinal
        }
    }

    return runningtotal;
  }

  armorInitiativePenalty() {
    const data = this.data;
    const armor = data.items.filter(function (item) {
      return item.type === 'armor' && item.data.data.worn === true;
    });
    const shield = data.items.filter(function (item) {
      return item.type === 'shield' && item.data.data.worn === true;
    });
    let penalty = 0;
    if (armor.length > 0) {
      penalty += armor[0].data.data.armorPenalty;
    }
    if (shield.length > 0) {
      penalty += shield[0].data.data.initiativepenalty;
    }

    return penalty;
  }

  karmaCalc() {
    const data = this.data;
    const namegiver = data.items.filter(function (item) {
      return item.type === 'namegiver';
    });
    let karmamod = 0;
    if (namegiver.length > 0) {
      karmamod = namegiver[0].data.data.karmamodifier;
    }
    const disciplines = this.items.filter(function (item) {
      return item.type === 'discipline';
    });
    let totalcircles = 0;

    for (const element of disciplines) {
      let discCircle = Number(element.data.data.circle);
      if (discCircle > totalcircles) {
        totalcircles = discCircle;
      }
    }

    let circles = totalcircles;
    return Number(circles * karmamod);
  }

  devotionCalc() {
   const data = this.data;
   const questor = this.items.filter(function (item) {
    return item.type === 'discipline';
   });
   let rank = 0;
   for (let i = 0; i < questor.length; i++) {
    if (questor[i].data.data.discipline === 'questor') {
      let discrank = questor[i].data.data.circle;
      rank = discrank;
      }
    }
    let questorrank = rank;
    return Number(questorrank * 10);
  }
  
  defenseCount(type) {
    const data = this.data;
    const shield = data.items.filter(function (item) {
      return item.type === 'shield' && item.data.data.worn === true;
    });
    let runningtotal = 0;
    let shieldvalue = 0;

    for (const element of shield) {
      shieldvalue = Number(element.data.data[type]);
      runningtotal += shieldvalue;
    }

    return runningtotal;
  }

  permanentMod(input) {
    const data = this.data;
    const namegiver = data.items.filter(function (item) {
      return item.type === 'namegiver';
    });
    let runningtotal = 0;
    for (let i = 0; i < namegiver.length; i++) {
      if (namegiver[i].data.data.bonus1.characteristic === input) runningtotal += namegiver[i].data.data.bonus1.value;
    }

    const disciplines = data.items.filter(function (item) {
      return (item.type === 'discipline' && item.data.data.discipline !== 'path');
    });
    const paths = data.items.filter(function (item) {
      return (item.type === 'discipline' && item.data.data.discipline === 'path');
    });
    let workingdiscipline;
    let disciplineBonus = 0;
    let workingpath;
    let p;
    let modifiedDisciplineName;
    let pathModifier = 0;
    let totalCircles = 0
    for (const element of disciplines) {
      let discCircle = Number(element.data.data.circle);
      if (discCircle > totalCircles) {
        totalCircles = discCircle;
      }
    }

    for (p = 0; p < paths.length; p++) {
      workingpath = paths[p].data;
      modifiedDisciplineName = workingpath.data.relatedDiscipline;
      var r;
      for (r = 1; r <= workingpath.data.circle; r++) {
        let characteristic = getProperty(workingpath, `data.circle${r}.characteristic`);
        if (characteristic === input && workingpath.data['circle' + r].value > pathModifier) {
        pathModifier = workingpath.data['circle' + r].value;
        }
      }
      
    }
    let d;
    for (d = 0; d < disciplines.length; d++) {
      workingdiscipline = disciplines[d].data;
      let modifiedValue;
      var r;
      for (r = 1; r <= workingdiscipline.data.circle; r++) {
        let characteristic = getProperty(workingdiscipline, `data.circle${r}.characteristic`);        
      
        if (characteristic === input){
          if (workingdiscipline.name === modifiedDisciplineName){
            modifiedValue = workingdiscipline.data['circle' + r].value + pathModifier;
          }
          else{
            modifiedValue = workingdiscipline.data['circle' + r].value;
          }
          if (characteristic === input && modifiedValue > disciplineBonus) {
            disciplineBonus = modifiedValue;
        }
 
        }
      }
    }
    runningtotal += disciplineBonus

    const threadItem = data.items.filter(function (item) {
      return (
        (item.type === 'weapon' || item.type === 'shield' || item.type === 'armor' || item.type === 'equipment') &&
        item.data.data.numberthreads > 0);
    });
    for (let i = 0; i < threadItem.length; i++) {
      let ranks = threadItem[i].data.data.numberthreads;
      let r;
      for (r = 1; r < ranks; r++) {
        let finalRank = `rank${r}`;
        if (threadItem[i].data.data.threads[finalRank].threadactive === true && threadItem[i].data.data.threads[finalRank].characteristic === input) {
          runningtotal += Number(threadItem[i].data.data.threads[finalRank].value);
        }
      }
    }

    const threads = data.items.filter(function (item) {
      return item.type === 'thread' && item.data.data.active === true;
    });
    for (let i = 0; i < threads.length; i++) {
      if (threads[i].data.data.characteristic === input) runningtotal += Number(threads[i].data.data.rank);
    }
    if (input === 'durability'){
      return (runningtotal * totalCircles)
    }
    else{
    return runningtotal;
    }
  }

  physicalDefensemod() {
    if (this.data.data.tactics.aggressive === true || this.data.data.tactics.knockeddown === true) {
      return -3;
    } else if (this.data.data.tactics.harried === true) {
      return -2;
    } else if (this.data.data.tactics.defensive === true) {
      return 3;
    } else {
      return 0;
    }
  }

  initiativeMod() {
    if (this.data.data.tactics.defensive) {
      return -3;
    } else {
      return 0;
    }
  }

  mysticDefensemod() {
    if (this.data.data.tactics.aggressive === true || this.data.data.tactics.knockeddown === true) {
      return -3;
    } else if (this.data.data.tactics.harried === true) {
      return -2;
    } else if (this.data.data.tactics.defensive === true) {
      return 3;
    } else {
      return 0;
    }
  }

  carryingCount() {
    const data = this.data;
    const stuff = data.items.filter(function (item) {
      return item.type === 'weapon' || item.type === 'armor' || item.type === 'shield' || item.type === 'equipment';
    });
    let runningtotal = 0;
    var i;
    for (i = 0; i < stuff.length; i++) {
      if (stuff[i].data.data.weight > 0) {
        runningtotal += Number(stuff[i].data.data.weight);
      }
    }
    return runningtotal;
  }

  bloodMagicCount() {
    const data = this.data;
    const stuff = data.items.filter(function (item) {
      return item.type === 'weapon' || item.type === 'armor' || item.type === 'shield' || item.type === 'equipment';
    });
    let runningtotal = 0;
    var i;
    for (i = 0; i < stuff.length; i++) {
      if (stuff[i].data.data.bloodMagicDamage > 0) {
        runningtotal += Number(stuff[i].data.data.bloodMagicDamage);
      }
    }
    return runningtotal;
  }

  carryingCapacity() {
    let namegiver = this.items.filter(function (item) {
      return item.type === 'namegiver';
    });
    let s = this.data.data.attributes.strengthvalue;
    if (namegiver.length > 0 && game.i18n.localize(namegiver[0].data.name) === 'Dwarf') {
      s += 2;
    }
    let t = Math.ceil(s / 5);
    return -12.5 * t ** 2 + 5 * t * s + 12.5 * t + 5;
  }

  getDice(step) {
    let dice = 0;
    if (game.settings.get('earthdawn4e', 'stepTableEdition') === 'step4') {
      var stepTable = [
        '0',
        '1d4-2',
        '1d4-1',
        '1d4',
        '1d6',
        '1d8',
        '1d10',
        '1d12',
        '2d6',
        '1d8+1d6',
        '2d8',
        '1d10+1d8',
        '2d10',
        '1d12+1d10',
        '2d12',
        '1d12+2d6',
        '1d12+1d8+1d6',
        '1d12+2d8',
        '1d12+1d10+1d8',
        '1d20+2d6',
        '1d20+1d8+1d6',
      ];
      if (step < 0) {
        return;
      } else if (step < 19) {
        dice = stepTable[step];
      } else {
        let i = step;
        let loops = 0;
        while (i > 18) {
          loops += 1;
          i -= 11;
        }
        dice = loops + 'd20+' + stepTable[i];
      }
    } else if (game.settings.get('earthdawn4e', 'stepTableEdition') === 'step1') {
      dice = get1eDice(step);
    } else if (game.settings.get('earthdawn4e', 'stepTableEdition') === 'step3') {
      dice = get3eDice(step);
    } else if (game.settings.get('earthdawn4e', 'stepTableEdition') === 'stepC') {
      dice = getCeDice(step);
    }
    return dice;
  }

  getStep(value) {
    if (!value > 0) {
      return 0;
    } else {
      return Number([Math.ceil(value / 3) + 1]);
    }
  }

  explodify(dice) {
    if (dice) {
      const regex = /(d[\d]+)/g;
      return dice.replace(regex, `$1x`);
    } else {
      return 0;
    }
  }

  async rollPrep(inputs) {
    console.log('[EARTHDAWN] Roll Prep Input', inputs);

    const weapon = inputs.weaponID ? this.items.get(inputs.weaponID) : null;
    const attackTalentID = inputs.attackTalentID
      ? inputs.attackTalentID
      : weapon
      ? await this.getAttack(weapon.data.data.weapontype)
      : null;
    const damageTalent = inputs.damageTalentID ? inputs.damageTalentID : null;
    const item = inputs.talentID ? this.items.get(inputs.talentID) : attackTalentID ? this.items.get(attackTalentID) : null;
    const att = inputs.attribute ? inputs.attribute: item ? item.data.data.attribute : weapon ? 'dexterityStep' : null;
    const modifier = inputs.modifier ? inputs.modifier : 0;
    const ranks = inputs.ranks ? inputs.ranks : item ? (item.data.data.finalranks ? item.data.data.finalranks : item.data.data.ranks ? item.data.data.ranks : 0) : 0;
    const title = inputs.talentName ? inputs.talentName : item ? item.name : game.i18n.localize(`earthdawn.${att.slice(0, 1)}.${att.slice(0, -4)}`);
    const type = item ? item.type : '';
    const rolltype = inputs.rolltype ? inputs.rolltype : '';
    const karma = inputs.karmaNumber ? inputs.karmaNumber : 0;
    const damageBonus = inputs.damageBonus ? inputs.damageBonus : 0
    //devotion
    const devotion = inputs.devotionNumber ? inputs.devotionNumber : 0;
    const initiative = inputs.initiative ? inputs.initiative : false;
    const difficulty = inputs.difficulty ? inputs.difficulty : rolltype === 'attack' ? await this.targetDifficulty() : 0;
    const strain = inputs.strain
      ? inputs.strain
      : item != null
      ? item.data.data.strain
      : this.data.data.tactics.aggressive && rolltype === 'attack'
      ? 1
      : 0;
    const spellName = inputs.spellName ? inputs.spellName : '';
    const damageId = damageTalent ? damageTalent : weapon ? inputs.weaponID : null;
    let karmarequested;
    if (karma > 0) {
      karmarequested = 'true';
    } else {
      karmarequested = this.data.data.usekarma;
    }
    
    let devotionrequested;
    if (devotion > 0) {
      devotionrequested = 'true';
    } else {
    
      devotionrequested = 'true';
    } 
   
    let devotionDie;
    if (this.data.data.devotion.max !== 0) {
      devotionDie = this.data.data.devotionDie;
    } else if (this.data.data.devotion.max === 0){
      devotionDie = 'd4';
    }

    inputs = {
      talent: title,
      actorId: this.id,
      type: type,
      modifier: modifier,
      attribute: att,
      ranks: ranks,
      karmarequested: karmarequested,
      devotionrequested: devotionrequested,
      devotionDie: devotionDie,
      strain: strain,
      difficulty: difficulty,
      weaponId: damageId,
      rolltype: rolltype,
      spellName: spellName,
      title: title,
      initiative: initiative,
      damageBonus: damageBonus
    };
    if (karma > 0) {
      inputs.karma = karma;
    } else {
      inputs.karma = karmaAllowed(inputs);
    }
    //devotion
    if (devotion > 0) {
      inputs.devotion = devotion;
    } else {
      inputs.devotion = devotionAllowed(inputs);
    }


    let miscmod = await optionBox(inputs);

    let basestep = this.data.data[inputs.attribute];
    if (!basestep) {
      ui.notifications.error(game.i18n.localize('earthdawn.a.attributeNotAssigned'));
    }

    inputs.karma = miscmod.karma;
    inputs.devotion = miscmod.devotion
    inputs.devotionDie = miscmod.devotionDie
    inputs.difficulty = miscmod.difficulty;
    inputs.finalstep = Number(basestep) + Number(inputs.ranks) + Number(miscmod.modifier);
    inputs.strain = miscmod.strain

    if ((this.data.data.tactics.defensive && inputs.rolltype !== "knockdown")|| (this.data.data.tactics.knockeddown === true && inputs.rolltype !== 'jumpUp')) {
      inputs.finalstep -= 3;
    } else if (this.data.data.tactics.aggressive === true  && rolltype === 'attack' && (weapon.data.data.weapontype === 'Melee' || weapon.data.data.weapontype === 'Unarmed') ) {
      inputs.finalstep += 3;
    } else if (this.data.data.tactics.harried === true) {
      inputs.finalstep -= 2;
    }

    return await this.rollDice(inputs);
  }

  async targetDifficulty() {
    const targets = Array.from(game.user.targets);
    var physicaldefense = 0;
    if (targets.length > 0) {
      physicaldefense = targets[0].actor.data.data.physicaldefense;
      if (targets[0].actor.type === 'creature' && targets[0].actor.data.data.tactics.harried === true) {
        physicaldefense -= 2;
      } else if (targets[0].actor.type === 'creature' && targets[0].actor.data.data.tactics.aggressive === true) {
        physicaldefense -= 3;
      }
      if (targets[0].actor.type === 'creature' && targets[0].actor.data.data.tactics.defensive === true) {
        physicaldefense += 3;
      }
    }
    return physicaldefense;
  }

  async rollDice(inputs) {
    console.log('[EARTHDAWN] Roll Inputs', inputs);

    let wounds = this.data.data.wounds;
    wounds = this.woundCalc(wounds);
    inputs.finalstep -= wounds;
    let dice = this.getDice(inputs.finalstep);
    inputs.karmaUsed = inputs.karma ? inputs.karma : 0;
    //devotion - what is this doing exactly?
    inputs.devotionUsed = inputs.devotion ? inputs.devotion : 0;
    let extraSuccess = 0;
    let karmaDie = this.data.data.karmaDie === 'S8' ? '2d6' : this.data.data.karmaDie === 'S9' ? 'd8+d6' : this.data.data.karmaDie === 'S10' ? '2d8' : this.data.data.karmaDie;
    let devotionDie = this.data.data.devotionDie;
    if (inputs.karmaUsed > 0 && inputs.devotionUsed == 0) {
      let karmaNew = this.data.data.karma.value - inputs.karmaUsed;
      if (karmaNew < 0) {
        ui.notifications.info(game.i18n.localize('earthdawn.k.karmaNo'));
        return false;
      }
      let karmaMultiplier = karmaDie.includes('2d') ? 2 : 1;
      let karmaDieType = karmaDie.includes('2d') ? karmaDie.slice(-2) : karmaDie;

      //Dice creation - Karma only
      dice += `+` + inputs.karmaUsed * karmaMultiplier + `${karmaDieType}`;
    } /*check for devotion */ else if (inputs.devotionUsed > 0 && inputs.karmaUsed == 0) {
      let devotionNew = this.data.data.devotion.value - inputs.devotionUsed;
      if (this.data.data.devotion.max > 0 && devotionNew < 0 ) {
        ui.notifications.info(game.i18n.localize('earhtdawn.d.devotionNo'));
        return false;
      }
      // Dice creation - Devotion only
      dice += `+` + inputs.devotionUsed + inputs.devotionDie;
    } /* check for devotion and karma */ else if (inputs.karmaUsed > 0 && inputs.devotionUsed > 0) {
      let karmaNew = this.data.data.karma.value - inputs.karmaUsed;
      let devotionNew = this.data.data.devotion.value - inputs.devotionUsed;
      if (karmaNew < 0) {
        ui.notifications.info(game.i18n.localize('earthdawn.k.karmaNo'));
        return false;
      }
      if (this.data.data.devotion.max > 0 && devotionNew < 0 ) {
        ui.notifications.info(game.i18n.localize('earhtdawn.d.devotionNo'));
        return false;
      }
      let karmaMultiplier = karmaDie.includes('2d') ? 2 : 1;
      let karmaDieType = karmaDie.includes('2d') ? karmaDie.slice(-2) : karmaDie;

      // Dice creation - Devotion and Karma
      dice += `+` + inputs.karmaUsed * karmaMultiplier + `${karmaDieType}`+ `+` + inputs.devotionUsed + inputs.devotionDie;
    }
    let formula = await this.explodify(dice);
    let r = new Roll(`${formula}`);

    await r.evaluate();

    console.log('[EARTHDAWN] Roll', r);

    if (r.roll) {
      if (inputs.karmaUsed > 0) {
        let karmaNew = this.data.data.karma.value - inputs.karmaUsed;
        await this.update({ 'data.karma.value': karmaNew });
      }
      
      //Remove devotion input value of option Box from current devotion points.
      if (inputs.devotionUsed > 0) {
        let devotionNew = this.data.data.devotion.value - inputs.devotionUsed;
        await this.update({ 'data.devotion.value': devotionNew });
      }
      

      if (inputs.strain > 0) {
        let damageNew = Number(this.data.data.damage.value) + Number(inputs.strain);
        await this.update({ 'data.damage.value': damageNew });
      }
    }
    
    

    if (inputs.difficulty > 0) {
      let margin = r.total - inputs.difficulty;
      inputs.margin = margin;
      inputs.extraSuccess = margin > 0 ? Math.floor(margin / 5) : 0;
    } else {
      inputs.extraSuccess = 0;
    }

    inputs.dice = dice;
    inputs.name = this.name;
    if (inputs.initiative === true){
      this.rollToInitiative(inputs,r);
    }

    chatOutput(inputs, r);
    return inputs;
  }

  getDurability() {
    const disciplines = this.items.filter(function (item) {
      return item.type === 'discipline';
    });
    disciplines.sort((a, b) => (a.data.data.durability > b.data.data.durability ? -1 : 1));
    let runningtotal = 0;
    let totalcircles = 0;
    var discCircle = 0;
    var discDura = 0;
    var threadDura = 0

    for (const element of disciplines) {
      if (Number(element.data.data.circle) - discCircle > 0) {
        discCircle = Number(element.data.data.circle) - discCircle;
        discDura = Number(element.data.data.durability);
      } else {
        discDura = 0;
      }

      let total = discDura * discCircle;
      runningtotal += total;
      if (Number(element.data.data.circle) - totalcircles > 0) {
        totalcircles = Number(element.data.data.circle);
      }
    }

    const threads = this.items.filter(function (item) {
      return item.type === 'thread' && item.data.data.active === true && item.data.data.characteristic === 'durability';
    });

    if (threads.length > 0){
      threadDura = threads[0].data.data.rank
    }

    return { runningtotal: runningtotal, totalcircles: totalcircles, threadDurability: (totalcircles * threadDura) };
  }

  getSpells() {
    const spells = this.items.filter(function (item) {
      return item.type === 'spell';
    });
  }

  getAttack(weapontype) {
    let attacktype = weapontype;
    let attackname = '';
    if (attacktype === 'Melee') {
      attackname = game.i18n.localize('earthdawn.m.melee_weapon');
    } else if (attacktype === 'Ranged') {
      attackname = game.i18n.localize('earthdawn.m.missile_weapon');
    } else if (attacktype === 'Thrown') {
      attackname = game.i18n.localize('earthdawn.t.thrown_weapon');
    } else if (attacktype === 'Unarmed') {
      attackname = game.i18n.localize('earthdawn.u.unarmed_combat');
    }

    let attack = this.items.filter(function (item) {
      return item.name === `${attackname}` && item.type === 'talent';
    });
    if (attack.length < 1) {
      attack = this.items.filter(function (item) {
        return item.name === `${attackname}` && item.type === 'skill';
      });
    }
    if (attack.length < 1) {
      return;
    } else {
      return attack[0].id;
    }
  }

  async halfMagic() {
    const inputs = {
      wounds: this.data.data.woundsCurrent,
    };
    const disciplines = this.items.filter(function (item) {
      return item.type === 'discipline';
    });
    var disciplinelist = `
    <label class="misc-mod-description">${game.i18n.localize('earthdawn.d.discipline')}</label>
    <select class="misc-mod-input" id='discipline_box'>
    `;
    var i;
    var disciplineId;
    var disciplineName;
    for (i = 0; i < disciplines.length; i++) {
      disciplineId = disciplines[i].data._id;
      disciplineName = disciplines[i].data.name;
      disciplinelist += `<option value="${disciplineId}">${disciplineName}</option>`;
    }
    disciplinelist += '</select>';

    const html = `
        <div>
            <div class="misc-mod">
                <label class="misc-mod-description">${game.i18n.localize('earthdawn.a.attribute')}: </label>
                <select id="attribute_box">
                    <option value="dexterityStep">Dexterity</option>
                    <option value="strengthStep">Strength</option>
                    <option value="toughnessStep">Toughness</option>
                    <option value="perceptionStep">Perception</option>
                    <option value="willpowerStep">Willpower</option>
                    <option value="charismaStep">Charisma</option>
                </select>
            </div>
            <div class="misc-mod">${disciplinelist}</div>
            <div class="misc-mod">
                <label class="misc-mod-description">${game.i18n.localize('earthdawn.m.modifierStep')}: </label>
                <input class="misc-mod-input circleInput" id="dialog_box" value=0 autofocus/>
            </div>
            <div class="misc-mod">
                <label class="misc-mod-description">${game.i18n.localize('earthdawn.d.difficulty')}: </label>
                <input class="misc-mod-input circleInput" id="difficulty_box" value=0 data-type="number" />
            </div>
        </div>
        <div >
            <div class="misc-mod">
                <label class="misc-mod-description">${game.i18n.localize('earthdawn.k.karma')}: </label>
                <input class="misc-mod-input circleInput" id="karma_box" value=0 data-type="number" />
            </div>
        </div>
        <div >
            <div class="misc-mod">
                <label class="misc-mod-description">${game.i18n.localize('earthdawn.d.devotion')}: </label>
                <input class="misc-mod-input circleInput" id="devotion_box" value=0 data-type="number" />
            </div>
        </div>
        <div>
            <select name="data.devotionDie" id="devotionDie_box" value=${inputs.devotionDie} {{#select data.data.devotionDie}}>
                <option value="na">na</option>
                <option value="d4">${game.i18n.localize ('earthdawn.d.d')}4</option>
                <option value="d6">${game.i18n.localize ('earthdawn.d.d')}6</option>
                <option value="d8">${game.i18n.localize ('earthdawn.d.d')}8</option>
                {{/select}}
            </select>
        </div>
        `;
    let miscmod = await new Promise((resolve) => {
      const sheet = this._sheet;
      new Dialog({
        title: game.i18n.localize('earthdawn.m.miscellaneousModifiers'),
        content: html,
        buttons: {
          ok: {
            label: game.i18n.localize('earthdawn.o.ok'),
            callback: (html) => {
              resolve({
                attribute: html.find('#attribute_box').val(),
                discipline: html.find('#discipline_box').val(),
                modifier: html.find('#dialog_box').val(),
                difficulty: html.find('#difficulty_box').val(),
                karma: html.find('#karma_box').val(),
                devotion: html.find('#devotion_box').val(),
                devotionDie: html.find('#devotionDie_box').val()
              });
            },
          },
        },
        default: 'ok',
      }).render(true);
    });

    let activediscipline = this.items.get(`${miscmod.discipline}`);
    let circle = Number(activediscipline.data.data.circle);

    let attributestep = Number(this.data.data[miscmod.attribute]);

    let basestep = Number(circle + attributestep + Number(miscmod.modifier));

    inputs.talent = `${game.i18n.localize('earthdawn.h.halfmagic')} (${activediscipline.data.name})`;
    inputs.karma = miscmod.karma;
    inputs.devotion = miscmod.devotion;
    inputs.devotionDie = miscmod.devotionDie;

    inputs.finalstep = basestep + Number(miscmod.modifier);
    if (this.data.data.tactics.defensive || this.data.data.tactics.knockeddown === true) {
      inputs.finalstep -= 3;
    }
    this.rollDice(inputs);
  }

  async knockdownTest(inputs) {
    if (this.type === 'creature') {
      inputs.difficulty = inputs.difficulty ? inputs.difficulty : 0;
      inputs.strain = inputs.strain ? inputs.strain : 0;
      inputs.karma = inputs.karma ? inputs.karma : 0;
      inputs.modifier = inputs.modifier ? inputs.modifier : 0;
      inputs.devotion = inputs.devotion ? inputs.devotion : 0;
      let miscmod = await optionBox(inputs);
      inputs = { finalstep: this.data.data.knockdown, rolltype: 'knockdown', talent: 'Knockdown', difficulty: miscmod.difficulty };
      let result = await this.rollDice(inputs);
      if (result.margin < 0) {
        this.update({ 'data.tactics.knockeddown': true });
      }
    } else {
      let difficulty = inputs.difficulty ? inputs.difficulty : 0;
      inputs = {
        attribute: 'strengthStep',
        rolltype: 'knockdown',
        name: 'Knockdown',
        difficulty: difficulty,
        title: game.i18n.localize('earthdawn.k.knockdownTest'),
      };
      let result = await this.rollPrep(inputs);
      console.log('[EARTHDAWN] Knockdown Test', inputs, result);
      if (result.margin < 0) {
        this.update({ 'data.tactics.knockeddown': true });
      }
    }
  }

  async jumpUpTest(inputs) {
    inputs = { attribute: 'dexterityStep', name: 'Jump Up', difficulty: 6, strain: 2, rolltype: 'jumpUp' };
    let results = await this.rollPrep(inputs);
    if (results.margin >= 0) {
      this.update({ 'data.tactics.knockeddown': false });
    } else if (results.margin < 0) {
      this.update({ 'data.tactics.knockeddown': true });
    }
  }

  async attuneMatrix(matrix) {
    let spellList = this.items.filter(function (item) {
      return item.type === 'spell' && item.data.data.circle <= matrix.data.data.circle;
    });
    let spellOptions = '';
    var arrayLength = spellList.length;
    for (var i = 0; i < arrayLength; i++) {
      let id = spellList[i].id;
      let name = spellList[i].name;
      spellOptions += `<option value='${id}'>${name}</option>`;
    }

    let html =
      '<select id= "spell-name" name="newspell">' +
      spellOptions +
      '</select><div><input type="checkbox" id="attune_fly">' +
      game.i18n.localize('earthdawn.a.attuneOnFly') +
      '</div>';

    let newspell = await new Promise((resolve) => {
      new Dialog({
        title: game.i18n.localize('earthdawn.m.matrixAttune'),
        content: html,
        buttons: {
          ok: {
            label: game.i18n.localize('earthdawn.o.ok'),
            callback: (html) => {
              resolve({
                id: html.find('#spell-name').val(),
                onfly: html.find('#attune_fly:checked'),
              });
            },
          },
          cancel: {
            label: game.i18n.localize('earthdawn.c.cancel'),
            callback: () => {},
          },
        },
        default: 'ok',
      }).render(true);
    });
    let spellarray = this.items.filter(function (item) {
      return item.id === `${newspell.id}`;
    });
    let spellinfo = spellarray[0];
    let discipline = this.getWeaving(spellinfo.data.data.discipline);
    discipline = game.i18n.localize(discipline)
    console.log(discipline)
    let itemarray = this.items.filter(function (item) {
      return item.name.includes(`${discipline}`) && item.type === 'talent';
    });
    if (itemarray.length < 1) {
      ui.notifications.error(
        `${game.i18n.localize('earthdawn.y.youDoNotHave')} ${discipline} - ${game.i18n.localize('earthdawn.a.attuneNot')}`,
      );
      return false;
    }
    let itemID = itemarray[0].id;
    if (newspell.onfly.length > 0) {
      let inputs = {
        difficulty: spellinfo.data.data.reattunedifficulty,
        talentID: itemID,
      };
      let returnvalue = await this.rollPrep(inputs);
      if (returnvalue.margin > 0) {
        matrix.update({
          'data.threadsrequired': spellinfo.data.data.threadsrequired,
          'data.targetadditionalthreads': 0,
          'data.range': spellinfo.data.data.range,
          'data.currentspell': spellinfo.name,
          'data.weavingdifficulty': spellinfo.data.data.weavingdifficulty,
          'data.castingdifficulty': spellinfo.data.data.castingdifficulty,
          'data.activethreads': 0,
          'data.discipline': spellinfo.data.data.discipline,
          'data.extrathreads': spellinfo.data.data.extrathreads,
          'data.extrasuccesses': spellinfo.data.data.extrasuccesses
        });
      } else {
        ui.notifications.info(`${game.i18n.localize('earthdawn.a.attuneFailed')}`);
      }
    } else {
      matrix.update({
        'data.threadsrequired': spellinfo.data.data.threadsrequired,
        'data.targetadditionalthreads': 0,
        'data.range': spellinfo.data.data.range,
        'data.currentspell': spellinfo.name,
        'data.weavingdifficulty': spellinfo.data.data.weavingdifficulty,
        'data.castingdifficulty': spellinfo.data.data.castingdifficulty,
        'data.activethreads': 0,
        'data.discipline': spellinfo.data.data.discipline,
        'data.extrathreads': spellinfo.data.data.extrathreads,
        'data.extrasuccesses': spellinfo.data.data.extrasuccesses
      });
    }
  }

  async weaveThread(matrix) {
    let thread_weaving = this.getWeaving(matrix.data.data.discipline);
    console.log(matrix.data.data.discipline)
    let localizedDiscipline = 'earthdawn.d.discipline' + matrix.data.data.discipline
    console.log(localizedDiscipline)
    let discipline_fallback = getProperty(game.i18n._fallback, `${localizedDiscipline}`)
    localizedDiscipline = game.i18n.localize(localizedDiscipline);
    console.log(localizedDiscipline);
    console.log(discipline_fallback);
    discipline_fallback = discipline_fallback === null ? game.i18n.localize(localizedDiscipline): discipline_fallback;
    let thread_weaving_fallback = getProperty(game.i18n._fallback, `${thread_weaving}`)
    thread_weaving_fallback = thread_weaving_fallback === null ? game.i18n.localize(thread_weaving) : thread_weaving_fallback;
    thread_weaving = game.i18n.localize(thread_weaving);
    let disciplineinfo = this.items.filter(function (item) {
      return item.type === `discipline` && (item.name.includes(`${localizedDiscipline}`) || item.name.includes(`${discipline_fallback}`));
    });
    let talent = this.items.filter(function (item) {
      return item.type === `talent` && (item.name.includes(`${thread_weaving}`) || item.name.includes(`${thread_weaving_fallback}`));
    });
    if (talent.length < 1) {
      ui.notifications.info(`${game.i18n.localize('earthdawn.y.youDoNotHave')} ` + thread_weaving + ' or ' + thread_weaving_fallback);
      return false;
    }
    
    let maxextra = 0;
    if (disciplineinfo.length > 0) {
      let label = game.i18n.localize('earthdawn.e.extraThreads');
      let label2 = game.i18n.localize('earthdawn.e.extraThreadEffects')
      let label3 = game.i18n.localize('earthdawn.e.extraThreadRemember')
      let label4 = game.i18n.localize('earthdawn.e.extra')
      if (matrix.data.data.targetadditionalthreads === 0 || matrix.data.data.targetadditionalthreads === null) {
        if (disciplineinfo.length > 0) {
          maxextra = Math.ceil(disciplineinfo[0].data.data.circle / 4);
        }
        let extraThreads = matrix.data.data.extrathreads;
        extraThreads = extraThreads.replace(/,/g, "</li><li>")
        extraThreads = "<ul><li>" + extraThreads + "</li></ul>"
         
        let html = `<div class="weave-thread-box">
                      <label>${label}</label>
                      <input class='circleInput' type='text' id='extra-threads' name='extra-threads' data-dtype='Number' />
                    
                      <br/>${label3} ${maxextra} ${label4}<br/>
                      <label>${label2}:</label>
                      ${extraThreads}
                    </div>`;

        let threadextra = await new Promise((resolve) => {
          new Dialog({
            title: game.i18n.localize('earthdawn.m.matrixWeave'),
            content: html,
            buttons: {
              ok: {
                label: game.i18n.localize('earthdawn.o.ok'),
                callback: (html) => {
                  resolve({
                    extrathreads: html.find('#extra-threads').val(),
                  });
                },
              },
              cancel: {
                label: game.i18n.localize('earthdawn.c.cancel'),
                callback: () => {},
              },
            },
            default: 'ok',
          }).render(true);
        });
        threadextra.extrathreads = threadextra.extrathreads > maxextra ? maxextra : threadextra.extrathreads;
        await matrix.update({ 'data.targetadditionalthreads': Number(threadextra.extrathreads) });
      }
    }
    let parameters = { talentID: talent[0].id, type: 'talent', difficulty: matrix.data.data.weavingdifficulty };
    let results = await this.rollPrep(parameters);
    if (Number(results.margin) >= 0) {
      let targetthreads = Number(matrix.data.data.threadsrequired) + Number(matrix.data.data.targetadditionalthreads);
      if (matrix.data.data.activethreads < targetthreads) {
        let newthreads = Number(matrix.data.data.activethreads) + 1 + Number(results.extraSuccess);
        newthreads = newthreads > targetthreads ? targetthreads : newthreads;
        await matrix.update({ 'data.activethreads': Number(newthreads) });
      }
    }
  }

  async castSpell(matrix) {
    if (matrix.data.data.totalthreadsneeded > matrix.data.data.totalthreads) {
      ui.notifications.info('Not Enough Threads. Keep Weaving!');
      return false;
    } else {
      let talent = this.items.filter(function (item) {
        return item.type === `talent` && (item.name === `Spellcasting` || item.name === `Spruchzauberei`);
      });
      if (talent.length < 1){
        ui.notifications.info('Character Does Not Have Spellcasting')
        return
      }
      var difficulty;
      const targets = Array.from(game.user.targets);
      if (
        (matrix.data.data.castingdifficulty === 'TMD' ||
          matrix.data.data.castingdifficulty === 'MVZ' ||
          matrix.data.data.castingdifficulty === 'MWST' ||
          matrix.data.data.castingdifficulty === 'MVK') &&
        matrix.data.data.range === 'Self'
      ) {
        difficulty = this.data.data.mysticdefense;
      } else if (
        (matrix.data.data.castingdifficulty === 'TMD' ||
          matrix.data.data.castingdifficulty === 'MVZ' ||
          matrix.data.data.castingdifficulty === 'MWST' ||
          matrix.data.data.castingdifficulty === 'MVK') &&
        targets.length > 0
      ) {
        difficulty = targets[0].actor.data.data.mysticdefense;
      } else if (!isNaN(matrix.data.data.castingdifficulty)) {
        difficulty = matrix.data.data.castingdifficulty;
      } else {
        difficulty = 0;
      }

      let inputs = {
        spellName: matrix.data.data.currentspell,
        talentID: talent[0].id,
        rolltype: 'spell',
        type: talent[0].type,
        difficulty: difficulty,
      };
      let results = await this.rollPrep(inputs);
      if (results) {
        await matrix.update({ 'data.activethreads': 0, 'data.targetadditionalthreads': 0 });
      }
    }
  }

  getWeaving(discipline) {
    let weavingTalent = '';

    switch (discipline) {
      case 'Elementalist':
        weavingTalent = 'earthdawn.e.elementalism';
        return weavingTalent;
      case 'Illusionist':
        weavingTalent = 'earthdawn.i.illusionism';
        return weavingTalent;
      case 'Nethermancer':
        weavingTalent = 'earthdawn.n.nethermancy';
        return weavingTalent;
      case 'Wizard':
        weavingTalent = 'earthdawn.w.wizardry';
        return weavingTalent;
      case 'Shaman':
        weavingTalent = 'earthdawn.s.shamanism';
        return weavingTalent;
    }
  }

  clearMatrix(matrix) {
    matrix.update({
      'data.currentspell': '',
      'data.weavingdifficulty': '',
      'data.targetadditionalthreads': '',
      'data.castingdifficulty': '',
      'data.activethreads': '',
      'data.threadsrequired': '',
    });
  }

  async weaponDamagePrep(weapon, extraSuccess, damageBonus) {
    let damage = '';
    let step = '';
    let damageStep = 0;
    let damageBonus2 = damageBonus;
    if (weapon.type === 'weapon') {
      damage = weapon.data.data.damageattribute;
      step = damage + 'Step';
    }
    else if (weapon.type === 'attack'){
      damageStep = weapon.data.data.damagestep;
    }
    else if (weapon.type === 'talent') {
      damage = weapon.data.data.attribute;
      step = damage;
      damageStep = weapon.data.data.final;
    }

    let att = this.data.data[step] ? Number(this.data.data[step]) : 0;
    if (weapon.type === 'talent') {
      damageStep = att + Number(weapon.data.data.finalranks);
      if (weapon.name === game.i18n.localize('Claw Shape')) {
        damageStep += 3;
      }
    } else if (weapon.type === 'weapon') {
      damageStep = att + Number(weapon.data.data.damageFinal);
    }
    let name = weapon.name;
    let inputs = {
      rolltype: 'weapondamage',
      damagestep: damageStep,
      extraSuccess: extraSuccess,
      talent: `${game.i18n.localize('earthdawn.d.damage')} (${name})`,
      damageBonus: damageBonus2
    };
    this.effectTest(inputs);
  }

  async effectTest(inputs) {
    console.log(inputs)
    let basestep = inputs.damagestep;
    let extraSuccess = 0;
    if (inputs.extraSuccess !== null) {
      extraSuccess = Number(inputs.extraSuccess * 2);
      if (inputs.damageBonus && (inputs.damageBonus !== null)){
      extraSuccess += Number(inputs.damageBonus);}
    }
    console.log(extraSuccess)

    let miscmod = await new Promise((resolve) => {
      new Dialog({
        title: game.i18n.localize('earthdawn.m.miscellaneousModifiers'),
        content: `
            <div class="misc-mod">
                <label class="misc-mod-description">${game.i18n.localize('earthdawn.m.modifierStep')}: </label>
                <input id="dialog_box" class="misc-mod-input circleInput" value="${extraSuccess}" autofocus/>
            </div>
            <div class="misc-mod">
                <label class="misc-mod-description">${game.i18n.localize('earthdawn.k.karma')}: </label>
                <input id="karma_box" class="misc-mod-input circleInput" value=0 data-type="number" />
            </div>`,
        buttons: {
          ok: {
            label: game.i18n.localize('earthdawn.o.ok'),
            callback: (html) => {
              resolve({
                modifier: html.find('#dialog_box').val(),
                karma: html.find('#karma_box').val(),
              });
            },
          },
        },
        default: 'ok',
      }).render(true);
    });

    let karmaused = 0;
    let karmanew = this.data.data.karma.value - miscmod.karma;
    if (karmanew < 0) {
      ui.notifications.info(game.i18n.localize('earthdawn.k.karmaNo'));
    } else if (miscmod.karma > 0) {
      karmaused = miscmod.karma;
    }

    inputs.finalstep = basestep + Number(miscmod.modifier);
    if (this.data.data.tactics.aggressive) {
      inputs.finalstep += 3;
    } else if (this.data.data.tactics.defensive) {
      inputs.finalstep -= 3;
    }
    let karmastring = '';
    if (karmaused > 0) {
      karmastring = `+${karmaused}${this.data.data.karmaDie}`;
    }
    let diceString = this.getDice(inputs.finalstep) + karmastring;
    let dice = this.explodify(diceString);
    let r = new Roll(`${dice}`);
    await r.roll();

    var results = {
      damagetype: inputs.damagetype,
      rolltype: inputs.rolltype,
      talent: inputs.talent,
      finalstep: inputs.finalstep,
      dice: diceString,
      name: this.name,
      result: String(r.result),
      total: r.total,
    };

    inputs.dice = dice;
    inputs.name = this.name;

    chatOutput(results, r);
    if (r.roll && karmaused > 0) {
      karmanew = this.data.data.karma.value - karmaused;
      this.update({ 'data.karma.value': karmanew });
      inputs.karmaused = karmaused;
    }
    return r.total;
  }

  async parseSpell(spellName, extraSuccess) {
    const spell = this.data.items.filter(function (item) {
      return item.type === 'spell' && item.name === spellName;
    });
    const spellEffect = spell[0].data.data.effect.toLowerCase();
    let talent = 'Effect';
    let damagestep;
    let steps = 0;
    let modifier = 0;
    let damagetype;
    let rolltype = 'spellEffect';
    if (spellEffect.includes('wil')) {
      steps = this.data.data.willpowerStep;
      if (spellEffect.includes('+')) {
        let regex = /([0-9])/g;
        let digitSearch = regex.exec(spellEffect);
        modifier = digitSearch[0];
      }
      if (spellEffect.includes('mystic') || spellEffect.includes('physical')) {
        rolltype = 'spelldamage';
        talent = 'Damage';
        if (spellEffect.includes('mystic')) {
          damagetype = 'mystic';
        } else if (spellEffect.includes('physical')) {
          damagetype = 'physical';
        }
      }
    }
    let willforce = game.i18n.localize('earthdawn.w.willforce');
    let willforceTalent = this.items.getName(willforce);

    damagestep = Number(steps) + Number(modifier);
    if (damagestep === 0) {
      ChatMessage.create({
        content: `<div>Effect:</div><div>${spell[0].data.data.effect}</div>`,
        speaker: ChatMessage.getSpeaker({ alias: this.name }),
      });
      return false;
    } else {
      if (this.items.getName(willforce)) {
        let miscmod = await new Promise((resolve) => {
          new Dialog({
            title: `game.i18n.localize("earthdawn.w.willforce")?`,
            content: `
                        <div>Do you want to use Willforce?</div>
                            `,
            buttons: {
              ok: {
                label: game.i18n.localize('earthdawn.y.yes'),
                callback: () => {
                  resolve({
                    willforce: true,
                  });
                },
              },
              no: {
                label: game.i18n.localize('earthdawn.n.no'),
                callback: () => {
                  resolve({
                    willforce: false,
                  });
                },
              },
            },
            default: 'ok',
          }).render(true);
        });
        if (miscmod.willforce === true) {
          damagestep += willforceTalent.data.data.finalranks;
          let newdamage = this.data.data.damage.value + 1;
          this.update({ 'data.damage.value': newdamage });
        }
      }

      let inputs = {
        damagetype: damagetype,
        rolltype: rolltype,
        damagestep: damagestep,
        extraSuccess: extraSuccess,
        talent: `${talent} (${spellName})`,
      };
      console.log(inputs)
      this.effectTest(inputs);
    }
  }


  async rollToInitiative(inputs, r){
    let array = Array.from(game.combat.combatants);
    let activeCombatant = array.filter(combatant => combatant.data.actorId === this.id);
    let combatantID = activeCombatant[0].id;

    await game.combat.setInitiative(combatantID, r.total)
  }

  async NPCtest(inputs) {
    inputs.wounds = Number(this.data.data.wounds);
    inputs.wounds = this.woundCalc(inputs.wounds);
    inputs.difficulty = inputs.difficulty ? inputs.difficulty : 0;
    const targets = Array.from(game.user.targets);
    inputs.difficulty = targets[0] ? targets[0].actor.data.data.physicaldefense : inputs.difficulty;
    let tactics = 0;
    if (this.data.data.tactics.aggressive === true) {
      inputs.strain = 1;
      tactics += 3;
    } else if (this.data.data.tactics.knockeddown === true) {
      tactics -= 3;
    } else if (this.data.data.tactics.harried === true) {
      tactics -= 2;
    } else if (this.data.data.tactics.defensive === true) {
      tactics -= 3;
    }
    inputs.devotion = 0;
    let miscmod = await optionBox(inputs);
    let basestep = inputs.steps;
    let karmaused = miscmod.karma;
    let karmaDie = this.data.data.karmaDie === 'S8' ? '2d6' : this.data.data.karmaDie === 'S10' ? '2d8' : this.data.data.karmaDie;
    let finalstep = basestep - inputs.wounds + tactics + Number(miscmod.modifier);
    let dice = await this.getDice(finalstep);
    let type = inputs.type;
    if (karmaused > 0) {
      let karmaMultiplier = karmaDie.includes('2d') ? 2 : 1;
      let karmaDieType = karmaDie.includes('2d') ? karmaDie.slice(-2) : karmaDie;
      dice += `+` + karmaused * karmaMultiplier + `${karmaDieType}`;
    }
    let extraSuccess = 0;
    let finaly = await this.explodify(dice);

    let r = new Roll(`${finaly}`);

    await r.evaluate();

    inputs.finalstep = finalstep;
    inputs.dice = dice;
    inputs.name = this.name;
    inputs.result = String(r.result);
    inputs.total = r.total;
    inputs.rolltype = type;
    inputs.weaponId = inputs.itemID;
    inputs.actorId = this.id;
    inputs.extraSuccess = 0;
    if (this.items.get(inputs.itemID).type === 'attack') {
      inputs.itemtype = 'attack';
      inputs.damage = this.items.get(inputs.itemID).data.data.damagestep;
    }

    if (r.roll && karmaused > 0) {
      let karmanew = this.data.data.karma.value - karmaused;

      // update function to subtract karma used from current karma
      this.update({ 'data.karma.value': karmanew });
      inputs.karmaused = karmaused;
    }

    if (r.roll && inputs.strain > 0) {
      let damagenew = this.data.data.damage.value + inputs.strain;
      this.update({ 'data.damage.value': damagenew });
    }

    if (miscmod.difficulty > 0) {
      let margin = r.total - miscmod.difficulty;
      inputs.margin = margin;
      if (margin > 0) {
        inputs.extraSuccess = Math.floor(margin / 5);
      }
    }

    chatOutput(inputs, r);
    return inputs;
  }

  async NPCDamage(actor, damage, extraSuccess) {
    let basestep = damage;
    extraSuccess = extraSuccess * 2;

    const html = `
    <div class="misc-mod">
        <label class="misc-mod-description">${game.i18n.localize('earthdawn.m.modifierStep')}: </label>
        <input id="dialog_box" class="misc-mod-input circleInput" value="${extraSuccess}" autofocus/>
    </div>
    <div class="misc-mod">
        <label class="misc-mod-description">${game.i18n.localize('earthdawn.k.karma')}: </label>
        <input id="karma_box" class="misc-mod-input circleInput" value=0 data-type="number" />
    </div>`;
    let miscmod = await new Promise((resolve) => {
      new Dialog({
        title: game.i18n.localize('earthdawn.m.miscellaneousModifiers'),
        content: html,
        buttons: {
          ok: {
            label: game.i18n.localize('earthdawn.o.ok'),
            callback: (html) => {
              resolve({
                modifier: html.find('#dialog_box').val(),
                karma: html.find('#karma_box').val(),
              });
            },
          },
        },
        default: 'ok',
      }).render(true);
    });

    let karmaused = 0;
    let karmanew = this.data.data.karma.value - miscmod.karma;
    if (karmanew < 0) {
      ui.notifications.info(game.i18n.localize('earthdawn.k.karmaNo'));
    } else {
      karmaused = miscmod.karma;
    }

    let finalstep = Number(basestep) + Number(miscmod.modifier);
    if (this.data.data.tactics.aggressive) {
      finalstep += 3;
    } else if (this.data.data.tactics.defensive) {
      finalstep -= 3;
    }
    let karmaUsedString = karmaused > 0 ? `+ ${karmaused}d6` : '';
    let diceString = this.getDice(finalstep) + karmaUsedString;
    let dice = this.explodify(diceString);

    let r = new Roll(`${dice}`);
    await r.roll();

    var results = {
      type: 'effect',
      rolltype: 'weapondamage',
      talent: 'Damage',
      finalstep: finalstep,
      dice: diceString,
      name: this.name,
      result: String(r.result),
      total: r.total,
    };
    chatOutput(results, r);
    if (r.roll && karmaused > 0) {
      karmanew = this.data.data.karma.value - karmaused;
      this.update({ 'data.karma.value': karmanew });
      inputs.karmaused = karmaused;
    }
  }

  async takeDamage(inputs) {
    let damage = inputs.damage;
    let type = inputs.type;
    let ignorearmor = inputs.ignorearmor;
    let damagetaken;
    let newdamage;

    if (isNaN(damage)){
      ui.notifications.error("No Damage Value Given");
      return false;
    }
    else{
    if (damage > 0 && type === 'physical') {
      if (ignorearmor === true) {
        damagetaken = damage;
      } else {
        damagetaken = damage - this.data.data.physicalarmor;
      }
    } else if (damage > 0 && type === 'mystic') {
      if (ignorearmor === true) {
        damagetaken = damage;
      } else {
        damagetaken = damage - this.data.data.mysticarmor;
      }
    } else {
      damagetaken = damage - this.data.data.physicalarmor;
    }

    if (damagetaken < 0) {
      damagetaken = 0;
    }

    if (isNaN(damagetaken)){
      ui.notifications.error("No Damage Value Given");
      return false;
    }
    else{
    newdamage = Number(this.data.data.damage.value) + Number(damagetaken);
    await this.update({ 'data.damage.value': newdamage });
    let armormessage =
      ignorearmor === true ? `${game.i18n.localize('earthdawn.a.armorIgnored')}` : `${game.i18n.localize('earthdawn.a.armorApplied')}`;
    let html = `<div>${this.name} ${game.i18n.localize('earthdawn.t.took')} ${damagetaken} ${armormessage}</div>`;
    var newwounds;

    await this.update({ 'data.damage.value': newdamage });

    if (this.data.data.damage.value >= this.data.data.damage.max) {
      ui.notifications.error(`${this.name} ${game.i18n.localize('earthdawn.i.isDead')}`);
      html += `<div>${this.name} ${game.i18n.localize('earthdawn.i.isDead')}!</div>`;
      if (damagetaken >= this.data.data.woundThreshold) {
        newwounds = this.data.data.wounds + 1;
        await this.update({ 'data.wounds': newwounds });
      }
    } else if (this.data.data.damage.value >= this.data.data.unconscious.max) {
      ui.notifications.error(`${this.name} ${game.i18n.localize('earthdawn.i.isUnconscious')}`);
      html += `<div>${this.name} ${game.i18n.localize('earthdawn.i.isUnconscious')}`;
      if (damagetaken >= this.data.data.woundThreshold) {
        newwounds = this.data.data.wounds + 1;
        await this.update({ 'data.wounds': newwounds, 'data.tactics.knockeddown': true });
      }
    } else {
      if (damagetaken >= this.data.data.woundThreshold) {
        newwounds = this.data.data.wounds + 1;
        await this.update({ 'data.wounds': newwounds });
        if (damagetaken >= this.data.data.woundThreshold + 5) {
          let knockdowndifficulty = damagetaken - this.data.data.woundThreshold;
          html += `<div>${this.name} ${game.i18n.localize('earthdawn.t.tookWound')}</div><div>${game.i18n.localize(
            'earthdawn.m.makeKnockDown',
          )} ${knockdowndifficulty}</div>`;
          let inputs = { difficulty: knockdowndifficulty };
          this.knockdownTest(inputs);
        }
      }
    }

    ChatMessage.create({
      content: html,
      speaker: ChatMessage.getSpeaker({ alias: name }),
    });
  }
}
  }

  async recoveryTest() {
    if (this.data.data.recoverytestscurrent < 1) {
      ui.notifications.info(game.i18n.localize('earthdawn.r.recoveryNoLeft'));
      return false;
    } else {
      let step = this.data.data.toughnessStep;
      let inputs = { damagestep: step, talent: 'Recovery Test' };
      let result = await this.effectTest(inputs);
      result = Number(result) - this.data.data.woundsCurrent;
      if (result < 1) {
        result = 1;
      }
      let newdamage = this.data.data.damage.value - result;
      newdamage = newdamage >= 0 ? newdamage : 0;
      let newrecovery = this.data.data.recoverytestscurrent - 1;
      this.update({ 'data.damage.value': newdamage, 'data.recoverytestscurrent': newrecovery });
    }
  }

  woundCalc(wounds){
    let resistPain = game.i18n.localize('earthdawn.r.resistpain')
    let fury = game.i18n.localize('earthdawn.f.fury')
    let finalwounds = wounds;
    if (this.data.items.filter((item) => item.name.includes(resistPain)).length > 0 && wounds > 0){
      ui.notifications.info("Character has Resist Pain (Calculated Automatically)");
      let resistPainName = this.data.items.filter((item) => item.name.includes(resistPain))[0].name
      let resistPainValue = Number(resistPainName.replace(/[^0-9]/g,''));
        finalwounds = (resistPainValue >= wounds) ? 0 : (Number(wounds) - Number(resistPainValue));
      }
    if ((this.data.items.filter((item) => item.name.includes(fury)).length > 0 && wounds > 0)){
      ui.notifications.info("Character has Fury (Calculated Automatically)");
      let furyName = this.data.items.filter((item) => item.name.includes(fury))[0].name
      let furyValue = Number(furyName.replace(/[^0-9]/g,''));
      if (wounds <= furyValue){
        finalwounds = -(wounds);

      }
    }
    return finalwounds;
  }

  async spellMatrixBox(inputs){

    const label1 = game.i18n.localize('earthdawn.a.attune');
    const label2 = game.i18n.localize('earthdawn.m.matrixWeaveRed');
    const label3 = game.i18n.localize('earthdawn.m.matrixCastRed');
    const label4 = game.i18n.localize('earthdawn.m.matrixClearRed');
    const label5 = game.i18n.localize('earthdawn.s.spell');
    const label6 = game.i18n.localize('earthdawn.s.spellThreads')
    const matrix = this.items.get(inputs.matrixID);
    const currentSpell = matrix.data.data.currentspell;
    const currentThreads = matrix.data.data.activethreads

    
    const html = `<p>${label5}: ${currentSpell}</p>
    <p>${label6}: ${currentThreads}</p>
    <select name="spellFunction" id="spellFunction">
    <option value="cast">${label3}</option>
    <option value="attune">${label1}</option>
    <option value="weave">${label2}</option>
    <option value="clear">${label4}</option>
    </select>`

    let option = await new Promise((resolve) => {
      new Dialog({
        title: inputs.matrixName,
        content: html,
        buttons: {
          ok: {
            label: game.i18n.localize('earthdawn.o.ok'),
            callback: (html) => {
              resolve({
                matrixFunction: html.find('#spellFunction').val(),
              });
            },
          },
        },
        default: 'ok',
      }).render(true);
    });

    if (option.matrixFunction === "attune"){
      this.attuneMatrix(matrix);
    } 
    else if (option.matrixFunction === "weave"){
      this.weaveThread(matrix)
    } 
    else if (option.matrixFunction === "cast"){
      this.castSpell(matrix)
    } 
    else if (option.matrixFunction === "clear"){
      this.clearMatrix(matrix)
    } 



    
    };


  async newDay() {
    if (this.data.data.damage.value > 0) {
      this.update({
        'data.karma.value': this.data.data.karma.max,
        'data.recoverytestscurrent': this.data.data.recoverytestsrefreshFinal,
      });
      this.recoveryTest();
      ui.notifications.info(game.i18n.localize('earthdawn.n.newDayRecovery'));
    } else if (this.data.data.wounds > 0 && this.data.data.damage.value === 0) {
      let newwounds = this.data.data.wounds - 1;
      let newtests = this.data.data.recoverytestsrefreshFinal - 1;
      this.update({
        'data.karma.value': this.data.data.karma.max,
        'data.recoverytestscurrent': newtests,
        'data.wounds': newwounds,
      });
    } else {
      this.update({
        'data.karma.value': this.data.data.karma.max,
        'data.recoverytestscurrent': this.data.data.recoverytestsrefreshFinal,
      });
    }
  }
}
