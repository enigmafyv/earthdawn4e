import earthdawn4ePCSheet from './sheets/earthdawn-4e-PC-sheet.js';
import earthdawn4eActor from './entities/earthdawn-4e-Actor.js';
import earthdawn4eItemSheet from './sheets/earthdawn-4e-item-sheet.js';
import earthdawn4eNPCSheet from './sheets/earthdawn-4e-NPC-sheet.js';
import earthdawn4eItem from './entities/earthdawn-4e-Item.js';
import { chatListeners } from './helpers/chat-listeners.js';
import { getDice } from './helpers/get-dice.js';
import earthdawn4eCombat from './entities/earthdawn-4e-combat.js';
import { preloadHandlebarTemplates } from './helpers/preload-handlebar-templates.js';
import { TakaItemSheet } from './sheets/taka-theme/taka-item-sheet.js';
import { registerHandlebarHelpers } from './helpers/handlebar-helpers.js';
import { registerGameSetting } from './helpers/register-game-setting.js';
import { registerPacks } from './helpers/register-modules.js'
import { TakaCharacterSheet } from './sheets/taka-theme/taka-character-sheet.js';
import { TakaCreatureSheet } from './sheets/taka-theme/taka-creature-sheet.js';
import {resourceBox} from './helpers/resource-box.js';

Hooks.once('init', function () {
  console.log('Initializing Earthdawn 4E for 0.8.6');

  // CONFIG.debug.hooks = true;
  CONFIG.Actor.documentClass = earthdawn4eActor;
  CONFIG.Item.documentClass = earthdawn4eItem;
  CONFIG.Combat.documentClass = earthdawn4eCombat;
  CONFIG.getDice = getDice;

  registerGameSetting();

  preloadHandlebarTemplates();

  registerHandlebarHelpers();

  game.initialize = initializePack;

  const brokenLinks = game.settings.settings.get('earthdawn4e.brokenLinks').choices[game.settings.get('earthdawn4e', 'brokenLinks')];
  console.log('[EARTHDAWN] Selected broken links', brokenLinks);
  if (brokenLinks === 'Hide broken links') {
    $('body').addClass('brokenLinks')
  }

  const theme = game.settings.settings.get('earthdawn4e.theme').choices[game.settings.get('earthdawn4e', 'theme')];
  console.log('[EARTHDAWN] Selected Theme', theme);

  Actors.unregisterSheet('core', ActorSheet);
  Actors.registerSheet('earthdawn4e', earthdawn4ePCSheet, {
    makeDefault: theme === 'earthdawn4e',
    types: ['pc', 'npc'],
  });
  Actors.registerSheet('earthdawn4e', earthdawn4eNPCSheet, {
    makeDefault: theme === 'earthdawn4e',
    types: ['creature'],
  });
  Actors.registerSheet('Takas Theme', TakaCharacterSheet, {
    makeDefault: theme === 'Takas Theme',
    types: ['pc', 'npc'],
  });
  Actors.registerSheet('Takas Theme', TakaCreatureSheet, {
    makeDefault: theme === 'Takas Theme',
    types: ['creature'],
  });

  Items.unregisterSheet('core', ItemSheet);
  Items.registerSheet('earthdawn4e', earthdawn4eItemSheet, {
    makeDefault: theme === 'earthdawn4e',
  });
  Items.registerSheet('Takas Theme', TakaItemSheet, {
    makeDefault: theme === 'Takas Theme',
  });

  Hooks.on('renderChatLog', (log, html) => chatListeners(html));

  Hooks.once('ready', () => {
    // Listen for dice icon click
    const diceIconSelector = '#chat-controls i.fas.fa-dice-d20';
    $(document).on('click', diceIconSelector, () => {
      getDice();
    });
    console.log('Sockets Ready');
    socket = socketlib.registerSystem('earthdawn4e');
    socket.register('damage', assignDamage);
    registerPacks();
    initializePack();
  });

  Hooks.on('preCreateItem', (item) => {
    if (!item.parent) return;

    const actor = item.parent;
    const namegivers = actor.items.filter(function (item) {
      return item.type === 'namegiver';
    });

    if (namegivers.length > 0 && item.type === 'namegiver') {
      ui.notifications.error(game.i18n.localize('earthdawn.n.namegiverAlready'));
      return false;
    }
  });

  Hooks.on('renderSidebarTab', (app, html) => {
    if (app instanceof Settings) {
      
      const button1Text = game.i18n.localize('earthdawn.l.linkMessage');
      const button2Text = game.i18n.localize('earthdawn.l.linkMessage2')
      let button1 = $(`<button><i class="fas fa-hand-holding-heart"/> ${button1Text}</button>`);
      let button2 = $(`<button><i class="fas fa-book"/> ${button2Text}</button>`);
      html.find('#settings-documentation').append(button1);
      html.find('#settings-documentation').append(button2);
      button1.click(() => {
        window.open('https://gitlab.com/fattom23/earthdawn4e/-/wikis/Home');
      });
      button2.click( async () => {
        let resourceInputs = {};
        resourceInputs.text = `<a class="link-item" href="https://fasagames.com" target="_blank">FASA Website</a><br/>
        <a class="link-item" href="http://pandagaminggrove.blogspot.com/" target="_blank">Panda Gaming Grove: Alternate Rules and Items from Morgan Weeks</a><br/>
        <a class="link-item" href="https://championschallenge.fasagames.com/comic/champions-challenge-1/" target="_blank">Champion's Challenge Webcomic</a><br/>
        <a class="link-item" href="https://anchor.fm/edsg-podcast" target="_blank">Earthdawn Survival Guide</a><br/>
        <a class="link-item" href="https://www.audible.com/pd/Namegivers-An-Earthdawn-Actual-Play-Podcast-Podcast/B08K58JNJX" target="_blank">Namegivers: An Earthdawn Actual Play Podcast</a><br/>
        <a class="link-item" href="https://www.youtube.com/channel/UCD7Ohx0WZkHuJS6UelxEM_w" target="_blank">Mystic Path Actual Play</a><br/>
        <a class="link-item" href="http://legendsofearthdawn.com/" target="_blank">Legends of Earthdawn Actual Play Podcast</a><br/>
        <a class="link-item" href="https://www.youtube.com/watch?v=z9O6kKGa4eM&t=1s" target="_blank">Relative Dimension Actual Play Videos</a><br/>
        <a class="link-item" href="https://www.youtube.com/channel/UCP3tmoj3OC6v3OGRkf7vQpw" target="_blank">Far Scholar Earthdawn</a><br/>
        `
        await resourceBox(resourceInputs);
      });
    }
  });

  Hooks.on('createItem', async (item) => {
    if (item.type === 'namegiver') {
      const actor = item.parent;
      actor.update({ 'data.movement': item.data.data.movement });
    }
    if (item.type === 'knack' && item.parent instanceof Actor) {
      let parentTalentName = item.data.data.sourceTalentName;
      const actor = item.parent;
      
      if (actor.items.getName(parentTalentName) === undefined){
        ui.notifications.error("You Don't Have the Proper Talent")
        return
      }
      let parentTalentID = actor.items.getName(parentTalentName).id
      let parentTalent = actor.items.get(parentTalentID);
      let parentTalentRanks = parentTalent.data.data.ranks
      if (parentTalentRanks < item.data.data.talentRequirement){
        ui.notifications.error("Not Enough Ranks in Parent Talent")
      }
      item.update({'data.sourceTalentId': parentTalentID});
    }
  });

  Hooks.on('hotbarDrop', async (hotbar, data, slot) => {
    if (data.data.type === "attack"){
      ui.notifications.error("Hotbar does not yet support creature attacks")
      return;
    }
    if (data.data.type !== 'spellmatrix'){
    let functionName = 'rollPrep';

    const itemID = data.data._id;
    let itemType = '';
    let rollType = '';
    if (data.data.type === 'talent' || data.data.type === 'skill') {
      itemType = 'talentID';
    } else if (data.data.type === 'weapon') {
      itemType = 'weaponID';
      rollType = 'attack';
    }

    const actorId = data.actorId;
    const macroData = {
      name: data.data.name,
      command: `
                let character = game.actors.get('${actorId}');
                let inputs = {${itemType}: '${itemID}', 'rolltype': '${rollType}'};
                character.${functionName}(inputs)`,
      img: data.data.img,
    };

    let macro = await Macro.create({
      name: macroData.name,
      type: 'script',
      img: macroData.img,
      command: macroData.command,
    });
    await game.user.assignHotbarMacro(macro, slot);
  }
  else {
    
    const itemID = data.data._id;
    const itemName = data.data.name;
    const functionName = "spellMatrixBox";
    const actorId = data.actorId;
    const macroData = {
      name: data.data.name,
      command: `
                let character = game.actors.get('${actorId}');
                let inputs = {matrixID: '${itemID}', matrixName: '${itemName}'};
                character.${functionName}(inputs)`,
      img: data.data.img,
    };

    let macro = await Macro.create({
      name: macroData.name,
      type: 'script',
      img: macroData.img,
      command: macroData.command,
    });
    await game.user.assignHotbarMacro(macro, slot);
}
  });

  Hooks.on('preCreateItem', async (document) => {
    console.log(document.data.img)
    if (document.parent && document.data.img !== `icons/svg/item-bag.svg`) return;
    else if (document.data.img === `icons/svg/item-bag.svg`){
    document.data.update({
      img: `systems/earthdawn4e/assets/${document.type}.png`,
    });
    return document;
  }
  });

  Hooks.on('preCreateActor', (document, createData) => {
    createData.type === 'creature' ? document.data.token.update({ actorLink: false }) : document.data.token.update({ actorLink: true });
  });

  async function assignDamage(inputs) {
    const targetToken = canvas.tokens.get(inputs.targetActor);
    const targetActor = targetToken.actor;
    await targetActor.takeDamage(inputs);
  }

  async function loadDisplay(introMessage, moduleName){
    return await new Promise((resolve) => {
      let box_title = `Welcome to ${moduleName}`;
      new Dialog({
        title: box_title,
        content: introMessage,
        buttons: {
          ok: {
            label: game.i18n.localize('earthdawn.o.ok'),
            callback: (html) => {
              resolve({
              });
            },
          },
        },
        default: 'ok',
      }).render(true);
    });
  }

  async function initializePack() {
    const obj = game.settings.get('earthdawn4e', 'packsInitialized');
    for (let module of Object.entries(obj)) {
      const moduleName = module[0];
      const workingModule = await game.modules.get(moduleName);
      let moduleLoaded = game.settings.get('earthdawn4e', 'packsInitialized')[moduleName].loaded;
      if (
        (game.modules.has(moduleName) && workingModule.active === true) &&
        (game.settings.get('earthdawn4e', 'packsInitialized')[moduleName].loaded === false ||
          game.settings.get('earthdawn4e', 'packsInitialized')[moduleName].moduleVersion < game.modules.get(moduleName).data.version)
      ) {
        console.log(moduleName)
        ui.notifications.info(`Attempting to load ${workingModule.data.title}`)
        let introMessage;
        await fetch(`modules/${moduleName}/imports.json`)
        .then(async (q) => q.json())
        .then(async (json) => {
          introMessage = json.loadMessage
        });
        if (introMessage != undefined){
        console.log(workingModule.data.title)
        await loadDisplay(introMessage, workingModule.data.title);
        }

        console.log(moduleName + ` Tried to Load`)
        let packs = Array.from(workingModule.packs);
        let compendiumImports = [];
        await fetch(`modules/${moduleName}/imports.json`)
            .then(async (q) => q.json())
            .then(async (json) => {
              compendiumImports = json.packs
            })
            console.log(compendiumImports)

        let compendiumName = `modules/${moduleName}/initialization.json`;
        if (await fetch(compendiumName)) {
          await fetch(compendiumName)
            .then(async (r) => r.json())
            .then(async (json) => {
              console.log(json);
              if (Object.keys(json).length !== 0){
              for (let folder of json){
              console.log(folder._id)
              const folderExists = folder._id;
              if (!game.folders.has(folderExists)) {
                ui.notifications.info(`Creating Folder: `+ folder.name)
                await Folder.createDocuments([folder], { keepId: true });
              }
              else{
                console.log(`${folder.name} Exists!`)
              }
            }
            }
            });
          for (let pack of packs) {
            if (compendiumImports != undefined){

            
            if (compendiumImports.includes(pack.name) || pack.name === 'locations') {
              console.log(pack)
              let joiner = '.';
              const packName = moduleName + joiner + pack.name;
              let packType = game.packs.get(packName).metadata.entity;
              const documents = await game.packs.get(packName).getDocuments();
              const documentIds = documents.map((a) => a.data._id);
              const documentsExist = documentIds.filter((id) => (game.journal.has(id) === true || game.actors.has(id) === true));
              if (packType === "JournalEntry"){
              await JournalEntry.deleteDocuments(documentsExist);
              console.log(documents)
              await JournalEntry.create(
                documents.map((c) => c.data),
                { keepId: true },
              );}
              else if (packType === "Actor"){
                await Actor.deleteDocuments(documentsExist);
              await Actor.create(
                documents.map((c) => c.data),
                { keepId: true },
              );
              }
            }
            }
          
          }
          ui.notifications.notify('Initializing Scenes');
          let m = game.packs.get(`${moduleName}.scene`);
          if (m) {
            let maps = await m.getDocuments();
            for (let map of maps){
              if(game.scenes.has(map.data._id)){
                await Scene.deleteDocuments([map.data._id])
              }
            }
            await Scene.create(maps.map((m) => m.data), {keepId: true}).then((sceneArray) => {
              sceneArray.forEach(async (s) => {
                let thumb = await s.createThumbnail();
                s.update({ thumb: thumb.thumb });
              });
            });
          }
          let settingAdjust = game.settings.get('earthdawn4e', 'packsInitialized');
          settingAdjust[moduleName].loaded = true;
          settingAdjust[moduleName].systemVersion = game.system.data.version;
          settingAdjust[moduleName].moduleVersion = workingModule.data.version;
          game.settings.set('earthdawn4e', 'packsInitialized', settingAdjust);
          console.log(game.settings.get('earthdawn4e', 'packsInitialized'));
        }
      } else {
        // console.log(`${moduleName} Is Current or Inactive`);
      }
    }
  }
});
