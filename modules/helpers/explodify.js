async function explodify(expression) {
  const regex = /(d[\d]+)/g;
  return expression.replaceAll(regex, `$1x`);
}
