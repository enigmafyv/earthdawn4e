export async function chatOutput(input, roll) {
  console.log('[EARTHDAWN] Chat Input', input);

  let rollMode = game.settings.get('core', 'rollMode');

  const title = `${input.name} ${game.i18n.localize('earthdawn.r.rolls')} ${input.talent}!`;
  const blind = rollMode === 'blindroll';
  const whisper = ['gmroll', 'blindroll'].includes(rollMode) ? ChatMessage.getWhisperRecipients('GM') : [];
  const speaker = ChatMessage.getSpeaker({ alias: name });
  const flavor = title + getKarma(input) +  getDevotion(input) + getRoll(input) + getSuccess(input) + getRuleOfOne(roll) + getOutcome(input) + getAttack(input);
  await roll.toMessage({ flavor, blind, whisper, speaker });
}

function getRuleOfOne(roll) {
  let Ones = 0;
  let notOnes = 0;
  roll.dice.forEach((set) => {
    set.results.forEach((dieResult) => {
      if (dieResult.result === 1) Ones++;
    });
  });
  roll.dice.forEach((set) => {
    set.results.forEach((dieResult) => {
      if (dieResult.result !== 1) notOnes++;
    });
  });

  let onlyOnes = game.i18n.localize('earthdawn.o.onlyOnes');
  return Ones > 1 && notOnes === 0 ? `<div class="rule-of-one">${onlyOnes}</div>` : '';
}

function getKarma(input) {
  return input.karmaUsed > 0
    ? `<div>
            ${game.i18n.localize('earthdawn.y.youUsed')} 
            ${input.karmaUsed} ${game.i18n.localize('earthdawn.k.karma')}
         </div>`
    : '';
}

function getDevotion(input) {
  return input.devotionUsed > 0
    ? `<div>
          ${game.i18n.localize('earthdawn.y.youUsed')} 
          ${input.devotionUsed} ${game.i18n.localize('earthdawn.d.devotion')}
      </div>`
    : '';
}

function getRoll(input) {
  return `
        <div>
            <span>${game.i18n.localize('earthdawn.s.step')}: ${input.finalstep}</span>
        </div>`;
}

function getSuccess(input) {
  return input.extraSuccess
    ? `<div class="extra-success">
            ${game.i18n.localize('earthdawn.y.youGot')} 
            ${input.extraSuccess} 
            ${game.i18n.localize('earthdawn.s.successExtra')}
        </div>`
    : '';
}

function getOutcome(input) {
  if ('margin' in input) {
    return input.margin >= 0
      ? `<div class="extra-success">${game.i18n.localize('earthdawn.s.success')}!</div>`
      : `<div class="rule-of-one">${game.i18n.localize('earthdawn.f.failure')}!</div>`;
  } else {
    return '';
  }
}

function getAttack(input) {
  let attackSection = '';
  if (input.rolltype === 'attack') {
    if (input.itemtype === 'attack') {
      attackSection = `<div>
                    <a class='damageRollNPC effectTest' data-actorId='${input.actorId}' data-damageStep='${
        input.damage
      }' data-extraSuccess='${input.extraSuccess}'>
                        ${game.i18n.localize('earthdawn.r.rollDamage')}
                        <i class='effectTest fas fa-dice'></i>
                    </a>
                </div>`;
    } else {
      attackSection = `<div>
                    <a class='damageRoll effectTest' data-actorId='${input.actorId}' data-weaponId='${input.weaponId}' data-damageBonus='${input.damageBonus}'data-extraSuccess='${
        input.extraSuccess
      }'>
                        ${game.i18n.localize('earthdawn.r.rollDamage')}
                        <i class='effectTest fas fa-dice'></i>
                    </a>
                </div>`;
    }
  } else if (input.rolltype === 'weapondamage' || input.rolltype === 'spelldamage') {
    attackSection = `<div>
                <a class='apply-damage' data-damageSource='${input.rolltype}' data-damageType='${input.damagetype}' data-damageTotal='${
      input.total
    }' >
                    ${game.i18n.localize('earthdawn.a.applyDamage')}
                    <i class='apply-damage fas fa-dice'></i>
                </a>
            </div>`;
  } else if (input.rolltype === 'spell') {
    attackSection = `<div>
                <a class='spellEffect effectTest' data-actorId='${input.actorId}' data-spellName='${input.spellName}' data-extraSuccess='${
      input.extraSuccess
    }'>
                    ${game.i18n.localize('earthdawn.e.effect')}
                    <i class='effectTest fas fa-dice'></i>
                </a>
            </div>`;
  }
  return attackSection;
}
