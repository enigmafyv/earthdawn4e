import { get1eDice } from './get-1e-dice.js';
import { get3eDice } from './get-3e-Dice.js';
import { getCeDice } from './get-ce-dice.js';

export async function getDice() {
  const label1 = game.i18n.localize('earthdawn.s.step');
  const label2 = game.i18n.localize('earthdawn.k.karma');
  const label3 = game.i18n.localize('earthdawn.d.devotion');
  const html = `
    ${label1}<input data-dtype="Number" id="stepNumber" /><br/>
    ${label2}<input data-dtype="Number" id="karmaDice" /><br/>
    ${label3}<input data-dtype="Number" id="devotionDice" />`;

  let step = await new Promise((resolve) => {
    new Dialog({
      title: game.i18n.localize('earthdawn.r.rollStep'),
      content: html,
      buttons: {
        ok: {
          label: game.i18n.localize('earthdawn.o.ok'),
          callback: (html) => {
            resolve({
              step: html.find('#stepNumber').val(),
              karma: html.find('#karmaDice').val(),
              devotion: html.find('#devotionDice').val(),
            });
          },
        },
      },
      default: 'ok',
    }).render(true);
  });

  const stepTable = [
    '0',
    '1d4-2',
    '1d4-1',
    '1d4',
    '1d6',
    '1d8',
    '1d10',
    '1d12',
    '2d6',
    '1d8+1d6',
    '2d8',
    '1d10+1d8',
    '2d10',
    '1d12+1d10',
    '2d12',
    '' + '1d12+2d6',
    '1d12+1d8+1d6',
    '1d12+2d8',
    '1d12+1d10+1d8',
    '1d20+2d6',
    '1d20+1d8+1d6',
  ];

  let dice = 0;
  let stepNum = step.step;
  if (game.settings.get('earthdawn4e', 'stepTableEdition') === 'step4') {
    if (stepNum < 0) {
      return;
    } else if (stepNum < 19) {
      dice = stepTable[stepNum];
    } else {
      let i = stepNum;
      let loops = 0;
      while (i > 18) {
        loops += 1;
        i -= 11;
      }
      dice = loops + 'd20+' + stepTable[i];
    }
  } else if (game.settings.get('earthdawn4e', 'stepTableEdition') === 'step1') {
    dice = get1eDice(stepNum);
  } else if (game.settings.get('earthdawn4e', 'stepTableEdition') === 'step3') {
    dice = get3eDice(stepNum);
  } else if (game.settings.get('earthdawn4e', 'stepTableEdition') === 'stepC') {
    dice = getCeDice(stepNum);
  }

  if (step.karma > 0) {
    dice += `+ ${step.karma}d6`;
  }
 //devotion - I'm not sure how to geht this done. I believe the regex part will replace the dice value... where is the function for that?
  if (step.devotion > 0 ) {
    dice += `+ ${step.devotion}d4`;
  }
  
  const regex = /(d[\d]+)/g;
  const result = dice.replace(regex, `$1x`);

  let r = new Roll(`${result}`);
  await r.roll();
  await r.toMessage(r);
}
