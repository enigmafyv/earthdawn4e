export async function testroll(inputs) {
  inputs.wounds = Number(this.data.data.woundsCurrent);
  inputs.ranks = inputs.ranks ? inputs.ranks : 0;
  inputs.difficulty = inputs.difficulty ? inputs.difficulty : 0;
  inputs.strain = inputs.strain ? inputs.strain : 0;

  inputs.karma = this.data.data.usekarma === 'true' && inputs.type === 'talent' ? 1 : 0;
  //devotion - shhall point to the devotion 
  inputs.devotion = this.data.data.devotionRequired === 'true' && inputs.type === 'devotion' ? 1 : 0;


  

  chatOutput(inputs);
  return inputs;
}
