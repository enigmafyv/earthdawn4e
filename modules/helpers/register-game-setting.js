import earthdawn4eItemSheet from '../sheets/earthdawn-4e-item-sheet.js';

export function registerGameSetting() {
  game.settings.register('earthdawn4e', 'stepTableEdition', {
    name: 'Step Table',
    hint: 'Which Step Table Do You Want to Use?',
    scope: 'world',
    type: String,
    default: 'step4',
    config: true,
    choices: {
      step4: 'Fourth Edition',
      step3: 'Third Edition',
      step1: 'First Edition',
      stepC: 'Classic Edition',
    },
  });

  game.settings.register('earthdawn4e', 'theme', {
    name: 'Theme',
    hint: 'Which theme do you want to use?',
    scope: 'world',
    type: String,
    default: 'earthdawn4e',
    config: true,
    choices: {
      earthdawn4e: 'earthdawn4e',
      taka: 'Takas Theme',
    },
  });

  game.settings.register('earthdawn4e', 'sortTalents', {
    name: 'Talent Sorting',
    hint: 'How would you like your talents to be sorted?',
    scope: 'world',
    type: String,
    default: 'default',
    config: true,
    choices: {
      default: 'Default',
      split: 'Split',
    },
  });

  game.settings.register('earthdawn4e', 'sortSpells', {
    name: 'Spell Sorting',
    hint: 'How would you like your spells to be sorted?',
    scope: 'world',
    type: String,
    default: 'default',
    config: true,
    choices: {
      default: 'Default',
      split: 'Split',
    },
  });

  //game setting for broken-link aktivation
  game.settings.register('earthdawn4e', 'brokenLinks', {
    name: 'Broken Links',
    Hint: 'do you want to show broken links as normal text?',
    //gibt es auch scope system???
    scope: 'world',
    type: String,
    default: 'default',
    config: true,
    choices: {
      default: 'Show broken links',
      hide: 'Hide broken links',
    }
  });

}

  