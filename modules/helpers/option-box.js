export async function optionBox(inputs) {
  return await new Promise((resolve) => {
    let box_title = inputs.title ? inputs.title : game.i18n.localize('earthdawn.m.miscellaneousModifiers');
    new Dialog({
      title: box_title,
      
      content: `
        <div class="misc-mod-popup">
          <div class="partial-three">
            <div class="misc-mod">
                <label class="misc-mod-description-option">${game.i18n.localize('earthdawn.m.modifierStep')}: </label>
                <input class="misc-mod-input circleInput" id="dialog_box" autofocus/>
            </div>
            <div class="misc-mod">
                <label class="misc-mod-description-option">${game.i18n.localize('earthdawn.d.difficulty')}: </label>
                <input class="misc-mod-input circleInput" id="difficulty_box" value=${inputs.difficulty} data-type="number" />
            </div>
            <div>
              <div class="misc-mod">
                  <label class="misc-mod-description-option">${game.i18n.localize('earthdawn.s.strainShort')}:</label>
                  <input class="misc-mod-input circleInput" id="strain_box" value=${inputs.strain} data-type="number" />
              </div>
            </div>
            <div class="misc-mod">
                <label class="misc-mod-description-option">${game.i18n.localize('earthdawn.k.karma')}: </label>
                <input class="misc-mod-input circleInput" id="karma_box" value=${inputs.karma} data-type="number" />
            </div>
            <div class="misc-mod">
                <label class="misc-mod-description-option">${game.i18n.localize('earthdawn.d.devotion')}: </label>
                <input class="misc-mod-input circleInput" id="devotion_box" value=${inputs.devotion} data-type="number" />
            </div>
          </div>
          <div>
            <div class="partial-three">
                <h4 class="centered">${game.i18n.localize('earthdawn.d.defenseTarget')}</h4> <br>
                <input type="radio" name="defense" value="none">
                <label for="none">${game.i18n.localize('earthdawn.n.none')}</label><br>
                <input type="radio" name="defense" value="physicaldefense">
                <label for="physical">${game.i18n.localize('earthdawn.d.defensePhysicalShort')}</label><br>
                <input type="radio" name="defense" value="mysticdefense">
                <label for="mystic">${game.i18n.localize('earthdawn.d.defenseMagicalShort')}</label><br>
                <input type="radio" name="defense" value="socialdefense">
                <label for="social">${game.i18n.localize('earthdawn.d.defenseSocialShort')}</label><br>
  
                <select id="devotionDie_box" value=${inputs.devotionDie} >
                <option value="na">na</option>
                <option value="d4">${game.i18n.localize ('earthdawn.d.d')}4</option>
                <option value="d6">${game.i18n.localize ('earthdawn.d.d')}6</option>
                <option value="d8">${game.i18n.localize ('earthdawn.d.d')}8</option>
                </select>

           
            </div>
          </div>
          <div>
            <div class="partial-three">
              <h4 class="centered">${game.i18n.localize('earthdawn.d.defenseGroup')}</h4>
              <input type="radio" name="targets" value="defenseHighest">
              <label for="defenseHighest">${game.i18n.localize('earthdawn.d.defeneseHighest')}</label><br>
              <input type="radio" name="targets" value="defenseHighest+#">
              <label for="defenseHighest+#">${game.i18n.localize('earthdawn.d.defeneseHighest#')}</label><br>
              <input type="radio" name="targets" value="defenseLowest">
              <label for="defenseLowest">${game.i18n.localize('earthdawn.d.defenseLowest')}</label><br>
              <input type="radio" name="targets" value="defenseLowest+#">
              <label for="defenseLowest+#">${game.i18n.localize('earthdawn.d.defenseLowest#')}</label><br>
            </div>
          </div>
        </div>
            `,
      buttons: {
        ok: {
          label: game.i18n.localize('earthdawn.o.ok'),
          callback: (html) => {
            resolve({
              modifier: html.find('#dialog_box').val(),
              difficulty: html.find('#difficulty_box').val() > 0 ? html.find('#difficulty_box').val() : findRadio(html),
              strain: html.find('#strain_box').val(),
              karma: html.find('#karma_box').val(),
              devotion: html.find('#devotion_box').val(),
              devotionDie: html.find('#devotionDie_box').val(),
              
            });

          },
        },
      },
      default: 'ok',
    }).render(true);
  
    
  });

}


function findRadio() {
  var radiosDefense = document.getElementsByName('defense');
  var radiosTarget = document.getElementsByName('targets');
  var targetDefense = '';
  var targetsCalc = '';
  for (var i = 0, length = radiosDefense.length; i < length; i++) {
    if (radiosDefense[i].checked) {
      // do whatever you want with the checked radio
      targetDefense = radiosDefense[i].value;
    }
  }

  if (targetDefense === '' || targetDefense === 'none') {
    return 0;
  }

  for (var s = 0, length = radiosTarget.length; s < length; s++) {
    if (radiosTarget[s].checked) {
      targetsCalc = radiosTarget[s].value;
    }
  }
  const targets = Array.from(game.user.targets).sort((a, b) =>
    a.data.document._actor.data.data[targetDefense] > b.data.document._actor.data.data[targetDefense] ? -1 : 1,
  );
  if (targets.length < 1) {
    ui.notifications.error('No Token Was Targeted - Difficulty Will Default to 0');
    return 0;
  }
  
  let targetDifficulty = 0;
  let targetLength = targets.length;
  if (targetsCalc === 'defenseHighest') {
    targetDifficulty = targets[0].data.document._actor.data.data[targetDefense];
  } else if (targetsCalc === 'defenseHighest+#') {
    targetDifficulty = Number(targets[0].data.document._actor.data.data[targetDefense]) + Number(targetLength - 1);
  } else if (targetsCalc === 'defenseLowest+#') {
    targetDifficulty = Number(targets[targetLength - 1].data.document._actor.data.data[targetDefense]) + Number(targetLength - 1);
  } else if (targetsCalc === 'defenseLowest') {
    targetDifficulty = Number(targets[targetLength - 1].data.document._actor.data.data[targetDefense]);
  } else {
    targetDifficulty = targets[0].data.document._actor.data.data[targetDefense];
  }

  return targetDifficulty;
}
